#include <WiFi.h>
#include <MQTT.h>
#include <Arduino.h>

#define LED_BUILTIN 2

const char SSID[] = "r0ch33nt3ign3n";
const char WLAN_PASSWORD[] = "PASSWORD";
const char MQTT_USERNAME[] = "mosquitto";
const char MQTT_PASSWORD[] = "PASSWORD";
const IPAddress MQTT_BROKER_IP = IPAddress(192, 168, 50, 85);

char clientId[13];
WiFiClient wifiClient;
MQTTClient mqttClient;
const int QOS = 1;

bool turned_on = false;
bool advertise = false;
bool advertise_status = false;

// How long the LED should blink
const int LED_TIMEOUT = 1000;
int led_start = 0;



const int outPin = 27;


void setup() {
  delay(500);
  pinMode(outPin, OUTPUT);
  Serial.begin(9600);
  Serial.println("setup..");
  delay(3000);
  setup_mqtt();
  Serial.println("setup..Done");
  digitalWrite(outPin, LOW);
}

void setup_mqtt() {

  Serial.println("Trying to connect to WiFi...");
  WiFi.begin(SSID, WLAN_PASSWORD);

  byte mac[6];
  WiFi.macAddress(mac);
  sprintf(clientId, "%02X%02X%02X%02X%02X%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

  // Note: Local domain names (e.g. "Computer.local" on OSX) are not supported
  // by Arduino. You need to set the IP address directly.
  mqttClient.begin(MQTT_BROKER_IP, 1883, wifiClient);
  mqttClient.onMessage(messageReceived);
  mqttClient.setWill("deregister", clientId, false, QOS);
  connect();
}

void connect() {

  delay(500);  // Used as a time window to upload new code in case of failure...
  Serial.println("CONNECTING!");

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Wifi Failed. retrying.");
  }
  Serial.println("Wifi Connected");
  Serial.println("MQTT connecting");

  delay(1000);
  while (!mqttClient.connect(clientId, MQTT_USERNAME, MQTT_PASSWORD)) {
    Serial.println("MTTQ Failed. retrying.");
    Serial.println(clientId);

    delay(1000);
  }
  advertise = true;
  Serial.println("\nconnected!");

    String publicTopic = "whosthere";
    String switchOnTopic = "switch/" + String(clientId) + "/turn_on";
    String switchOffTopic = "switch/" + String(clientId) + "/turn_off";
    String toggleTopic = "switch/" + String(clientId) + "/toggle";


    mqttClient.subscribe(publicTopic.c_str(), QOS);
    mqttClient.subscribe(switchOnTopic.c_str(), QOS);
    mqttClient.subscribe(switchOffTopic.c_str(), QOS);
    mqttClient.subscribe(toggleTopic.c_str(), QOS);


}

void loop() {
  // Make sure we are connected
  if (!mqttClient.connected()) {
    connect();
  }

  // Handle all mqtt messages
  mqttClient.loop();  


  // If we have to communicate if we are online... do it
  if (advertise) {
    mqttClient.publish("register/switch", clientId, false, QOS);
    advertise = false;
  }


  // If we have to communicate our state... do it
  if (advertise_status) {
    if (turned_on) {
      String topic = "switch/" + String(clientId) + "/on";
      mqttClient.publish(topic.c_str(), "", false, QOS);
    } else {
      String topic = "switch/" + String(clientId) + "/off";
      mqttClient.publish(topic.c_str(), "", false, QOS);
    }
    advertise_status = false;
  }

  // We blink for one second if we got a whosthere...
  if (led_start != 0 && millis() - led_start > LED_TIMEOUT) {
    digitalWrite(LED_BUILTIN, LOW);
    led_start = 0;
  }

    
}


  void messageReceived(String &topic, String &payload) {
  // Note: Do not use the mqttClient in the callback to publish, subscribe or
  // unsubscribe as it may cause deadlocks when other things arrive while
  // sending and receiving acknowledgments. Instead, change a global variable,
  // or push to a queue and handle it in the loop after calling `mqttClient.loop()`.

  if (topic == "whosthere") {
    digitalWrite(LED_BUILTIN, HIGH);
    led_start = millis();
    advertise = true;
  } else if (topic == "switch/" + String(clientId) + "/turn_on") {
    turnOn();
  } else if (topic == "switch/" + String(clientId) + "/turn_off") {
    turnOff();
  } else if (topic == "switch/" + String(clientId) + "/toggle") {
    toggle();
  } 
  else {
    Serial.println("\nUnknown Message!" + topic + payload);
  }
}

void turnOn() {
  if (turned_on) {return;}
  digitalWrite(outPin, HIGH);
  turned_on = true;
  advertise_status = true;
}

void turnOff() {
  if (!turned_on) {return;}
  digitalWrite(outPin, LOW);
  turned_on = false;
  advertise_status = true;
}

void toggle() {
    if (turned_on) {
      digitalWrite(outPin, LOW);
      turned_on = false;
    } else {
      digitalWrite(outPin, HIGH);
      turned_on = true;
    }
    advertise_status = true;
}

