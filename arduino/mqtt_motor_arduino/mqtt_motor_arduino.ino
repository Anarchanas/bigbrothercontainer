#include <WiFi.h>
#include <MQTT.h>

const char SSID[] = "r0ch33nt3ign3n";
const char WLAN_PASSWORD[] = "WLAN_PASSWORD";
const char MQTT_USERNAME[] = "mosquitto";
const char MQTT_PASSWORD[] = "PASSWORD";
const IPAddress MQTT_BROKER_IP = IPAddress(192, 168, 50, 85);

char clientId[13];
WiFiClient wifiClient;
MQTTClient mqttClient;
bool advertise = true;
const int QOS = 1;


// Motor related data
int pwm = 10;
int pin_motor_forward = 9;
int pin_motor_backward = 8;
int btn_bottom = A0;
int btn_top = A1;
const uint FULL_VELOCITY = 255;
const int LED_TIMEOUT = 1000;
uint hit_bottom_sensor_timeout = 0;
int led_start = 0;

// MOTOR STATUS
bool on_top = false;
bool on_bottom = false;
bool opening = false;
bool closing = false;
bool advertise_status = false;
uint velocity = 0;



void setup() {
  delay(500);
  Serial.begin(9600);
  Serial.println("setup..");
  delay(3000);
  setup_mqtt();
  setup_motor();
  Serial.println("setup..Done");
}

void loop() {
  if (!mqttClient.connected()) {
    connect();
  }

  mqttClient.loop();  // Handles all mttq msgs

  if (advertise) {
    mqttClient.publish("register/motor", clientId, false, QOS);
    advertise = false;
  }

  if (advertise_status) {
    if (opening) {
      String topic = "motor/" + String(clientId) + "/opening";
      char buffer[10];
      itoa(velocity, buffer, 10);
      mqttClient.publish(topic.c_str(), buffer, false, QOS);
    } 

    else if (closing) {
      String topic = "motor/" + String(clientId) + "/closing";
      char buffer[10];
      itoa(velocity, buffer, 10);
      mqttClient.publish(topic.c_str(), buffer, false, QOS);
    }

    else if (on_top) {
      String topic = "motor/" + String(clientId) + "/hit_top";
      char buffer[10];
      itoa(velocity, buffer, 10);
      mqttClient.publish(topic.c_str(), buffer, false, QOS);
    }

    else if (on_bottom) {
      String topic = "motor/" + String(clientId) + "/hit_bottom";
      char buffer[10];
      itoa(velocity, buffer, 10);
      mqttClient.publish(topic.c_str(), buffer, false, QOS);
    }

    else if (!opening && !closing) {
      String topic = "motor/" + String(clientId) + "/stopped";
      char buffer[10];
      itoa(velocity, buffer, 10);
      mqttClient.publish(topic.c_str(), buffer, false, QOS);
    }

    advertise_status = false;
  }


  // We blink for one second if we got a whosthere...
  if (led_start != 0 && millis() - led_start > LED_TIMEOUT) {
    digitalWrite(LED_BUILTIN, LOW);
    led_start = 0;
  }

  hitTop();
  hitBottom();

}

void setup_mqtt() {

  Serial.println("Trying to connect to WiFi...");
  WiFi.begin(SSID, WLAN_PASSWORD);

  byte mac[6];
  WiFi.macAddress(mac);
  sprintf(clientId, "%02X%02X%02X%02X%02X%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);



  // Note: Local domain names (e.g. "Computer.local" on OSX) are not supported
  // by Arduino. You need to set the IP address directly.
  mqttClient.begin(MQTT_BROKER_IP, 1883, wifiClient);
  mqttClient.onMessage(messageReceived);
  mqttClient.setWill("deregister", clientId, false, QOS);
  connect();
}


void setup_motor() {
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(pwm, OUTPUT);
  pinMode(pin_motor_forward, OUTPUT);
  pinMode(pin_motor_backward, OUTPUT);
  pinMode(btn_bottom, INPUT_PULLUP);
  pinMode(btn_top, INPUT_PULLUP);
}

void connect() {

  delay(500);  // Used as a time window to upload new code in case of failure...
  Serial.println("CONNECTING!");

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Wifi Failed. retrying.");
  }
  Serial.println("Wifi Connected");
  Serial.println("MQTT connecting");

  delay(1000);
  while (!mqttClient.connect(clientId, MQTT_USERNAME, MQTT_PASSWORD)) {
    Serial.println("MTTQ Failed. retrying.");
    Serial.println(clientId);

    delay(1000);
  }
  advertise = true;
  Serial.println("\nconnected!");

  String publicTopic = "whosthere";
  String openTopic = "motor/" + String(clientId) + "/open";
  String closeTopic = "motor/" + String(clientId) + "/close";
  String stopTopic = "motor/" + String(clientId) + "/stop";

  mqttClient.subscribe(publicTopic.c_str(), QOS);
  mqttClient.subscribe(openTopic.c_str(), QOS);
  mqttClient.subscribe(closeTopic.c_str(), QOS);
  mqttClient.subscribe(stopTopic.c_str(), QOS);
  mqttClient.publish("register/motor", clientId, false, QOS);
}

void messageReceived(String &topic, String &payload) {
  // Note: Do not use the mqttClient in the callback to publish, subscribe or
  // unsubscribe as it may cause deadlocks when other things arrive while
  // sending and receiving acknowledgments. Instead, change a global variable,
  // or push to a queue and handle it in the loop after calling `mqttClient.loop()`.

  if (topic == "whosthere") {
    digitalWrite(LED_BUILTIN, HIGH);
    led_start = millis();
    advertise = true;
  } else if (topic == "motor/" + String(clientId) + "/stop") {
    stopMotor();

  } else if (topic == "motor/" + String(clientId) + "/open") {
    uint _velocity;
    memcpy(&_velocity, payload.c_str(), sizeof(_velocity));
    spinUp(_velocity);

  } else if (topic == "motor/" + String(clientId) + "/close") {
    uint _velocity;
    memcpy(&_velocity, payload.c_str(), sizeof(_velocity));
    spinDown(_velocity);
  } else if (topic == "motor/" + String(clientId) + "/hit_bottom_timeout") {
    uint timeout;
    memcpy(&timeout, payload.c_str(), sizeof(timeout));
    hit_bottom_sensor_timeout = timeout;
  } 
  else {
    Serial.println("\nUnknown Message!" + topic + payload);
  }
}

void change_bottom_sensor_timeout(int timeout){
  hit_bottom_sensor_timeout = timeout;
}

void spinUp(uint _velocity) {
  digitalWrite(pin_motor_forward, LOW);
  digitalWrite(pin_motor_backward, LOW);
  digitalWrite(pin_motor_forward, HIGH);
  analogWrite(pwm, _velocity);
  velocity = _velocity;
  closing = false;
  opening = true;
  advertise_status = true;
}

void spinDown(uint _velocity) {
  digitalWrite(pin_motor_forward, LOW);
  digitalWrite(pin_motor_backward, LOW);
  digitalWrite(pin_motor_backward, HIGH);
  analogWrite(pwm, _velocity);
  velocity = _velocity;
  closing = true;
  opening = false;
  advertise_status = true;

}

void stopMotor() {
  digitalWrite(pin_motor_forward, LOW);
  digitalWrite(pin_motor_backward, LOW);
  analogWrite(pwm, 0);
  velocity = 0;
  closing = false;
  opening = false;
  advertise_status = true;

}

void hitBottom() {
  if (digitalRead(btn_bottom) == LOW) {
    if (!on_bottom) {
      on_bottom = true;
      delay(hit_bottom_sensor_timeout);
      stopMotor();
    }   
  } else {
    on_bottom = false;
  }

}

void hitTop() {
 if (digitalRead(btn_top) == LOW) {
    if (!on_top) {
      on_top = true;
      stopMotor();
    }
  } else {
    on_top = false;
  }
}
