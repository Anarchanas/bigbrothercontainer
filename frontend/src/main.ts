/**
 * Vue3 Main script
 */

// Load vue core
import store from '@/store';
import { createApp } from 'vue';
import VueNativeSock from "vue-native-websocket-vue3";
import App from '@/App.vue';
import vuetify from '@/plugins/vuetify';
import router from '@/router';
import { useSocketStoreWithOut } from './store/UseSocketStore';


const BACKEND_URL: string = import.meta.env.VITE_APP_BACKEND_URL;

/** Register Vue */
const vue = createApp(App);
const piniaSocketStore = useSocketStoreWithOut(vue);

vue.use(router);
vue.use(vuetify);

vue.use(
  VueNativeSock,
  `ws://${BACKEND_URL}/ws`,
  {
    store: piniaSocketStore,
    format: "json",
  //  connectManually: true,
    reconnection: true,
    reconnectionAttempts: 10,
    reconnectionDelay: 3000
  }
);

// Run!
router
  .isReady()
  .then(() => vue.mount('#app'))
  .catch(e => console.error(e));
