import { defineStore } from 'pinia'
import { computed, reactive, ref } from 'vue'
import { useLightCommander } from '@/composables/LightCommander'
import { CountdownTimer } from '@/composables/CountdownTimer'


export const useDeviceStore = defineStore('devices', () => {

    const applicationState = reactive({'state':''} as any)
    const device_map = ref({} as any)
    const allDeviceIds = ref(new Set<string>)
    const remainingTransitionTimes = ref(new Map<string, [number, number]>)
    const selectedMotors = ref(new Set<string>)
    const selectedLights = ref(new Set<string>)

    const { sendWledCommand } = useLightCommander()
    const groupIds = ref(new Set<string>)
    const recentlyUpdatedLights = new Map<string, number>
    const selectedPresets = ref(new Map<string, number>)
    const countdownTimers = new Map<string, CountdownTimer>

    const selectedSwitches = ref(new Set<string>)


    /* --------- SELECTION -------------- */

    function selectDevice(id:string) {
        let device = device_map.value[id]
        if (device.type === 'motor') {
            selectedMotors.value.add(id);
        } else if (device.type === 'light') {
            selectedLights.value.add(id)
        } else if (device.type === 'switch') {
            selectedSwitches.value.add(id)
        }
    }

    function isDeviceSelected(id: string) {
        return computed(() => {
            let device = device_map.value[id]
            if (device.type === 'motor') {
                return selectedMotors.value.has(id);
            } else if (device.type === 'light') {
                return selectedLights.value.has(id)
            } else if (device.type === 'switch') {
                return selectedSwitches.value.has(id)
            }
        })
    }

    function deselectDevice(id:string) {
        let device = device_map.value[id]
        if (device.type === 'motor') {
            selectedMotors.value.delete(id);
        } else if (device.type === 'light') {
            selectedLights.value.delete(id)
        } else if (device.type === 'switch') {
            selectedSwitches.value.delete(id)
        }
    }

    function selectAll() {
        for (let deviceId of allDeviceIds.value) {
            selectDevice(deviceId)
        }
    }

    function deselectAll() {
        for (let deviceId of allDeviceIds.value) {
            deselectDevice(deviceId)
        }
    }

    /* --------------- GROUPS --------------------- */


    function getByGroup(id:String | undefined) {
        let deviceIdsPerGroup =  [...allDeviceIds.value].filter((mqtt_id) => device_map.value[mqtt_id].group == id )
        let devicesPerGroup = deviceIdsPerGroup.map((id) => device_map.value[id])
        devicesPerGroup.sort((a,b) => {
            if (a.name === null && b.name === null) {
                return 0; // both are equal
            }
            if (a.name === null) {
                return 1; // a should come after b
            }
            if (b.name === null) {
                return -1; // b should come after a
            }
            return a.name.localeCompare(b.name)
        })
        return devicesPerGroup.map((device: any) => device.mqtt_id)
    }

    /* --------------- APPLICATION STATE --------------------- */


    function setApplicationState(_appState:any) {
        applicationState['state'] = ref(_appState['state'])
    }



    /* ------------- MOTORS --------------------------- */

    function addMotor(motor:any) {
        groupIds.value.add(motor.group)
        device_map.value[motor.mqtt_id] = motor
        allDeviceIds.value.add(motor.mqtt_id)

    }

    /* ------------- Switches --------------------------- */

    function addSwitch(switchDevice:any) {
        groupIds.value.add(switchDevice.group)
        device_map.value[switchDevice.mqtt_id] = switchDevice
        allDeviceIds.value.add(switchDevice.mqtt_id)
    }



    /* ------------- LIGHTS --------------------------- */

    /* Makes sure that we dont send to many commands at a time */
    function hasRecentlyBeenUpdated(mqtt_id:string) {
        let currentDate = new Date().getTime()
        if (!(recentlyUpdatedLights.has(mqtt_id))) {
            recentlyUpdatedLights.set(mqtt_id, currentDate)
            
            return false
        }

        let lastUpdated = recentlyUpdatedLights.get(mqtt_id)
        if (currentDate - lastUpdated! > 100) {
            recentlyUpdatedLights.set(mqtt_id, currentDate)
            return false
        } else {
            return true
        }
    }

    function updateTime(mqtt_id:string) {
        return (minutes:number, seconds:number) => {
            remainingTransitionTimes.value.set(mqtt_id, [minutes, seconds])
        }
    }

    function addLight(light:any) {
        groupIds.value.add(light.group)
        device_map.value[light.mqtt_id] = light
        allDeviceIds.value.add(light.mqtt_id)
        selectedPresets.value.set(light.mqtt_id, light.default_special_preset)

        if (! countdownTimers.get(light.mqtt_id)) {
            countdownTimers.set(light.mqtt_id, new CountdownTimer(updateTime(light.mqtt_id), ()=>{}, 0))
        }

        if (light.transition > 20) {
            let countdownTimer = countdownTimers.get(light.mqtt_id)
            countdownTimer?.setCountdown((light.transition+5) / 10)
            countdownTimer?.start()
        }

    }

    function addBrightness(mqtt_id:string, delta:number) {

        if (Math.abs(delta) < 1) {
            return
        }

        let device = device_map.value[mqtt_id]
        let updated_brightness = device.brightness + delta

        if (device.type != 'light') {
            return
        } 
        setBrightness(mqtt_id, updated_brightness, 3)
    }

    function setBrightness(mqtt_id:string, brightness:number, transitionTime: number) {
        if (hasRecentlyBeenUpdated(mqtt_id)) {return}
        if (brightness < 0){ brightness = 0} 
        else if (brightness > 255){ brightness = 255 }
       


        let device = device_map.value[mqtt_id]

        if (device.type != 'light') {
            return
        }
        device.brightness = brightness
        device.transition = transitionTime

        if (device.turned_on) {
            let cmd = {'brightness':brightness, 'command':'static', 'mqtt_id':mqtt_id, "transition":transitionTime}
            sendWledCommand(cmd)
        } else if (!device.on) {
        }
    }

    function turn_on_off(on:boolean, mqtt_id:string) {
        on = !on;
        let cmd;
        let device = device_map.value[mqtt_id]

        cmd = {'command':"static", "mqtt_id":mqtt_id, 'transition':1}
        sendWledCommand(cmd)
        cmd = {"temperature":device.temperature, "brightness":device.brightness,
        'command':"static", "turned_on":on, "mqtt_id":mqtt_id}
        device.turned_on = on
        sendWledCommand(cmd)
      }



      function addTemperature(mqtt_id:string, delta:number) {
        if (Math.abs(delta) < 1) {
            return
        }

        let device = device_map.value[mqtt_id]
        let updated_temperature = device.temperature + delta

        if (device.type != 'light') {
            return
        } 

        setTemperature(mqtt_id, updated_temperature, 3)
    }




    function setTemperature(mqtt_id:string, temperature:number, transitionTime:number) {
        if (hasRecentlyBeenUpdated(mqtt_id)) {return}
        if (temperature < 0 || temperature > 255){
            return
        }
        let device = device_map.value[mqtt_id]
        if (device.type != 'light') {
            return
        }
        device.temperature = temperature
        device.transition = transitionTime

        if (device.turned_on) {
            let cmd = {'temperature':temperature, 'command':'static', 'mqtt_id':mqtt_id, "transition":transitionTime}
            sendWledCommand(cmd)
        }
    }



    return { 
        addMotor, 
        getByGroup, setApplicationState,
        applicationState,
        isDeviceSelected,
        selectDevice,
        deselectDevice,
        selectAll,
        deselectAll,
        selectedMotors,
        selectedLights,
        selectedSwitches,
        addLight,
        addSwitch,
        setBrightness,
        setTemperature,
        turn_on_off,
        device_map,
        groupIds, 
        selectedPresets,
    addBrightness, addTemperature, remainingTransitionTimes,
}
  })

  export default useDeviceStore