import { defineStore } from 'pinia';
import type { App } from 'vue';

import type { SocketStore } from '@/store/SocketTypes';
import { setupStore } from '@/store/SetupPinia';
import { useDeviceStore } from '@/store/DeviceStore';
import {useGlobal} from '@/store';



export const useSocketStore = (app: App<Element>) => {
  return defineStore({
    id: 'socket',
    state: (): SocketStore => ({
      isConnected: false,
      message: '',
      reconnectError: false,
      heartBeatInterval: 60000,
      heartBeatTimer: 0,
    }),
    actions: {
      SOCKET_ONOPEN(event: any) {
        console.log('successful websocket connection');
        app.config.globalProperties.$socket = event.currentTarget;
        this.isConnected = true;
        this.heartBeatTimer = window.setInterval(() => {
          const message = Date();
          this.isConnected &&
            app.config.globalProperties.$socket.sendObj({
              type: 'poll',
              data: {message: message},
            });
        }, this.heartBeatInterval);
      },
      SOCKET_ONCLOSE(event: any) {
        this.isConnected = false;
        window.clearInterval(this.heartBeatTimer);
        this.heartBeatTimer = 0;
        console.log('websocket disconnect: ' + new Date());
        console.log(event);
      },
      SOCKET_ONERROR(event: any) {
        console.error(event);
      },
      SOCKET_ONMESSAGE(message: any) {
        console.log(message)
        message = JSON.parse(message);
        
        if (message.type === 'motor') {
          let deviceStore = useDeviceStore()
          deviceStore.addMotor(message.data);
        } 
        else if (message.type === 'loop') {
          let deviceStore = useDeviceStore()
          deviceStore.setApplicationState(message.data)
        } else if (message.type === 'light') {
          let deviceStore = useDeviceStore()
          deviceStore.addLight(message.data)
        } else if (message.type === 'switch') {
          let deviceStore = useDeviceStore()
          deviceStore.addSwitch(message.data)
        }
        
        
        else if (message.type === 'error') {
          let globalStore = useGlobal();
          globalStore.setMessage(message['data']['error'] + "  " + message['data']['message'])
        }
        this.message = message;
      },
      SOCKET_RECONNECT(count: any) {
        console.info('trying to reconnect...', count);
      },
      SOCKET_RECONNECT_ERROR() {
        this.reconnectError = true;
      },
    },
  })();
};

// Need to be used outside the setup
export function useSocketStoreWithOut(app: App<Element>) {
  setupStore(app);
  return useSocketStore(app);
}
