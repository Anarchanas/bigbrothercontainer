export interface SocketStore {
  isConnected: boolean;
  message: string;
  reconnectError: boolean;
  heartBeatInterval: number;
  heartBeatTimer: number;
}

export interface socketType {
  $connect: () => void;
}
