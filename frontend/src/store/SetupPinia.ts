import { createPinia } from 'pinia';
import type { App } from 'vue';

const store = createPinia();

/**
 * Creates a pinia instance and adds it to the store.
 * @param app - The app the store should be attached to
 */
export function setupStore(app: App<Element>) {
  app.use(store);
}

export { store };
