
class CountdownTimer {
    callback : Function
    intervalId: any = -1
    targetTime: number
    duration: number
    updateDisplay : Function
 
    constructor(updateDisplay: Function, callback: Function, duration: number) {
    this.callback = callback;
    this.duration = duration
    const currentTime = new Date().getTime();
    this.updateDisplay = updateDisplay;
    this.targetTime = currentTime + duration * 1000 // duration in seconds
  }

  setCountdown(seconds:number) {
    this.duration = seconds
    const currentTime = new Date().getTime();
    this.targetTime = currentTime + this.duration * 1000 // duration in seconds
  }

  start() {

    if (this.intervalId != -1) {
      return; // Timer is already running.
    }
    this.setCountdown(this.duration)

    const currentTime = new Date().getTime();
    const remainingTime = this.targetTime - currentTime
    const remainingMinutes = Math.floor(remainingTime / 60000);
    const remainingSeconds = Math.floor((remainingTime % 60000) / 1000);
    this.updateDisplay(remainingMinutes, remainingSeconds);


    this.intervalId = setInterval(() => {
        const currentTime = new Date().getTime();
        const remainingTime = this.targetTime - currentTime;

      if (remainingTime <= 0) {
        this.stop();
        this.callback();

      } else {
        const remainingMinutes = Math.floor(remainingTime / 60000);
        const remainingSeconds = Math.floor((remainingTime % 60000) / 1000);
        this.updateDisplay(remainingMinutes, remainingSeconds);
      }
    }, 900);
  }

  stop() {
    if (this.intervalId) {
      clearInterval(this.intervalId);
      this.intervalId = -1;
    }
  }
}

class StopWatch {
  timeInSeconds: number = 0
  updateDisplay: Function
  running: boolean = false
  intervalId: any = -1


  constructor(updateDisplay: Function) {
    this.updateDisplay = updateDisplay
  }

  start() {

    if (this.intervalId != -1) {
      return; // Timer is already running.
    }
    this.updateDisplay(Math.floor(this.timeInSeconds / 60000), Math.floor((this.timeInSeconds % 60000) / 1000))

    this.intervalId = setInterval(() => {
        this.timeInSeconds += 1000;
        const minutes = Math.floor(this.timeInSeconds / 60000);
        const seconds = Math.floor((this.timeInSeconds % 60000) / 1000);

        this.updateDisplay(minutes, seconds);
    }, 1000);
  }
  
  stop() {
    if (this.intervalId) {
      clearInterval(this.intervalId);
      this.intervalId = -1;
    }
  }
}

const getCurrentDateTime = () => {
  const now = new Date();
  const year = now.getFullYear();
  // Add 1 because JavaScript months are 0-based.
  const month = String(now.getMonth() + 1).padStart(2, '0');
  const day = String(now.getDate()).padStart(2, '0');

  const hours = String(now.getHours()).padStart(2, '0');
  const minutes = String(now.getMinutes()).padStart(2, '0');
  const seconds = String(now.getSeconds()).padStart(2, '0');

  return `${hours}:${minutes}:${seconds} - ${day}.${month}.${year}`;
}

export { CountdownTimer,  getCurrentDateTime, StopWatch }
