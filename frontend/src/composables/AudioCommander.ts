const BACKEND_URL: string = import.meta.env.VITE_APP_BACKEND_URL;

export function useAudioCommander() {

    function getAvailableSongs() {
        return fetch(`http://${BACKEND_URL}/api/audioplayer/songs`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json', 
          },
        })
        .then(response => {
            if (!response.ok) {
              throw new Error(`Request failed with status ${response.status}`);
            }
            return response.json();
          })
          .then(data => { return data } )
          .catch(error => {
            console.error(error);
          });
      }

      function getForeground() {
        return fetch(`http://${BACKEND_URL}/api/audioplayer/foreground`, {
          method: 'GET',
        })
        .then(response => {
            if (!response.ok) {
              throw new Error(`Request failed with status ${response.status}`);
            }
            return response.json();
          })
          .then(data => { return data } )
          .catch(error => {
            console.error(error);
          });
      }

      function setForeground(foregroundConfig: any, successCallback?: (response: any) => void, errorCallback?: (error: any) => void) {
        fetch(`http://${BACKEND_URL}/api/audioplayer/foreground`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json', 
          },
          body: JSON.stringify(foregroundConfig), 
        })
          .then(response => {
            if (!response.ok) {
              throw new Error(`Request failed with status ${response.status}`);
            }
            return response.json();
          })
          .then(data => { if (successCallback) { successCallback(data) } } )
          .catch(error => {
            console.error(error);
            if (errorCallback) {
              errorCallback(error); 
            }
          });
      }


      function getBackground() {
        return fetch(`http://${BACKEND_URL}/api/audioplayer/background`, {
          method: 'GET',
        })
          .then(response => {
            if (!response.ok) {
              throw new Error(`Request failed with status ${response.status}`);
            }
            return response.json();
          })
          .then(data => { return data } )
          .catch(error => {
            console.error(error);
          });
      }

      function setBackground(backgroundConfig: any, successCallback?: (response: any) => void, errorCallback?: (error: any) => void) {
        fetch(`http://${BACKEND_URL}/api/audioplayer/background`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json', 
          },
          body: JSON.stringify(backgroundConfig), 
        })
          .then(response => {
            if (!response.ok) {
              throw new Error(`Request failed with status ${response.status}`);
            }
            return response.json();
          })
          .then(data => { if (successCallback) { successCallback(data) } } )
          .catch(error => {
            console.error(error);
            if (errorCallback) {
              errorCallback(error); 
            }
          });
      }

    return {  
        getAvailableSongs,
        getBackground,
        setBackground,
        getForeground,
        setForeground,
      };
}
