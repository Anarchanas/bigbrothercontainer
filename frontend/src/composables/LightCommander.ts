import type { StringTypeAnnotation } from '@babel/types';
import { getCurrentInstance } from 'vue';


const BACKEND_URL: string = import.meta.env.VITE_APP_BACKEND_URL;

interface LightCommand {
    command: string
    mqtt_id: string
    brightness?: number
    temperature?: number
    preset?: number
    feature?: number
    group?: number
    turned_on?: boolean
    transition?: number
}

export function useLightCommander() {
  // Get the current component instance
  const vm = getCurrentInstance();

  if (!vm) {
    throw new Error("This function can only be used within `setup()`");
  }

  // Access WebSocket from global properties
  const socket = vm.appContext.config.globalProperties.$socket;


  function sendWledCommand(cmd: LightCommand) {
    let request = {'type':'light', 'data':cmd}
    socket.send(JSON.stringify(request));
  }

  function updateLight(mqtt_id: string, lightUpdate: any, successCallback?: (response: any) => void, errorCallback?: (error: any) => void) {
    fetch(`http://${BACKEND_URL}/api/lights/${mqtt_id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json', 
      },
      body: JSON.stringify(lightUpdate), 
    })
      .then(response => {
        if (!response.ok) {
          throw new Error(`Request failed with status ${response.status}`);
        }
        return response.json();
      })
      .then(data => { if (successCallback) { successCallback(data) } } )
      .catch(error => {
        console.error(error);
        if (errorCallback) {
          errorCallback(error); 
        }
      });
  }

  function activatePreset(mqtt_id:string, preset:number){
    sendWledCommand({"preset":preset, "mqtt_id":mqtt_id, "command":"preset", 'transition':1})
  }

  function deactivatePreset(mqtt_id:string){
    sendWledCommand({"preset":1, "mqtt_id":mqtt_id, "command":"preset", 'transition':0})
  }


  return {  
    sendWledCommand,
    updateLight,
    activatePreset,
    deactivatePreset
  };
}