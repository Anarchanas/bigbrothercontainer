import type { StringTypeAnnotation } from '@babel/types';
import { getCurrentInstance } from 'vue';


const BACKEND_URL: string = import.meta.env.VITE_APP_BACKEND_URL;

export function useMotorCommander() {
  // Get the current component instance
  const vm = getCurrentInstance();

  if (!vm) {
    throw new Error("This function can only be used within `setup()`");
  }

  // Access WebSocket from global properties
  const socket = vm.appContext.config.globalProperties.$socket;

  function open(mqttId:String, velocity:number) {
    if (socket && socket.readyState === WebSocket.OPEN) {
        if (velocity > 255 || velocity < 0) {
            console.error("Only values between 0 and 255 allowed...");
            return;
        }

       let data = {
        type:"motor", 
        data: {command: "open", mqtt_id: mqttId, velocity: velocity}
        }
      socket.send(JSON.stringify(data));
    }
  }

  function close(mqttId:String, velocity:number) {
    if (socket && socket.readyState === WebSocket.OPEN) {
        if (velocity > 255 || velocity < 0) {
            console.error("Only values between 0 and 255 allowed...");
            return;
        }

       let data = {
        type:"motor", 
        data: {command: "close", mqtt_id: mqttId, velocity: velocity}
        }
      socket.send(JSON.stringify(data));
    }
  }


  function stop(mqttId:String) {
    if (socket && socket.readyState === WebSocket.OPEN) {
       let data = {
        type:"motor", 
        data: {command: "stop", mqtt_id: mqttId}
        }
      socket.send(JSON.stringify(data));
    }
  }


  function updateMotor(mqtt_id: string, motorUpdate: any, successCallback?: (response: any) => void, errorCallback?: (error: any) => void) {
    fetch(`http://${BACKEND_URL}/api/motors/${mqtt_id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json', 
      },
      body: JSON.stringify(motorUpdate), 
    })
      .then(response => {
        if (!response.ok) {
          throw new Error(`Request failed with status ${response.status}`);
        }
        return response.json();
      })
      .then(data => { if (successCallback) { successCallback(data) } } )
      .catch(error => {
        console.error(error);
        if (errorCallback) {
          errorCallback(error); 
        }
      });
  }


  return {  
    open,
    stop,
    close,
    updateMotor
  };
}