import type { StringTypeAnnotation } from '@babel/types';
import { getCurrentInstance } from 'vue';


const BACKEND_URL: string = import.meta.env.VITE_APP_BACKEND_URL;

export function useDeviceCommander() {
  // Get the current component instance
  const vm = getCurrentInstance();

  if (!vm) {
    throw new Error("This function can only be used within `setup()`");
  }

  // Access WebSocket from global properties
  const socket = vm.appContext.config.globalProperties.$socket;


  function getAllDevices(){
    fetch(`http://${BACKEND_URL}/api/devices`, {method: 'GET'})
    .then(response => console.log(response))
    .catch(error => console.error(error));
  }

  function updateDevice(mqtt_id: string, deviceUpdate: any, successCallback?: (response: any) => void, errorCallback?: (error: any) => void) {
    fetch(`http://${BACKEND_URL}/api/devices/${mqtt_id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json', 
      },
      body: JSON.stringify(deviceUpdate), 
    })
      .then(response => {
        if (!response.ok) {
          throw new Error(`Request failed with status ${response.status}`);
        }
        return response.json();
      })
      .then(data => { if (successCallback) { successCallback(data) } } )
      .catch(error => {
        console.error(error);
        if (errorCallback) {
          errorCallback(error); 
        }
      });
  }


  function deleteDevice(mqtt_id: string, successCallback?: (response: any) => void, errorCallback?: (error: any) => void) {
    fetch(`http://${BACKEND_URL}/api/devices/${mqtt_id}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json', 
      },
    })
      .then(response => {
        if (!response.ok) {
          throw new Error(`Request failed with status ${response.status}`);
        }
        return response.json();
      })
      .then(data => { if (successCallback) { successCallback(data) } } )
      .catch(error => {
        console.error(error);
        if (errorCallback) {
          errorCallback(error); 
        }
      });
  }

  

  return {  
    getAllDevices,
    updateDevice,
    deleteDevice
  };
}