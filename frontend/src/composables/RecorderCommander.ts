
const BACKEND_URL: string = import.meta.env.VITE_APP_BACKEND_URL;
import { getCurrentInstance } from 'vue';


export function useRecorderCommander() {
  // Get the current component instance
  const vm = getCurrentInstance();

  if (!vm) {
    throw new Error("This function can only be used within `setup()`");
  }

    function loopCommand(command: String, recordingIds: [number] | null, name: String | null = null) {
        return {'type': 'loop', "command" : command, "recording_ids": recordingIds, "name": name}
    }
    const socket = vm.appContext.config.globalProperties.$socket;

    function play(recordingIds: [number] | null) {
        let data = loopCommand('play', recordingIds);
        let message = {'type':'loop', 'data':data}
        socket.send(JSON.stringify(message));
    }

    function playForever(recordingIds: [number] | null) {
        let data = loopCommand('play_forever', recordingIds)
        let message = {'type':'loop', 'data':data}
        socket.send(JSON.stringify(message));
    }
    function record(name: String | undefined, recordingIds: [number] | null) {
        let data = loopCommand('record', recordingIds, name)
        let message = {'type':'loop', 'data':data}
        socket.send(JSON.stringify(message));
    }


    function stop() {
        let data = loopCommand('stop', null)
        let message = {'type':'loop', 'data':data}
        socket.send(JSON.stringify(message));
    }

    function initialState() {
        let data = loopCommand('initial_state', null)
        let message = {'type':'loop', 'data':data}
        socket.send(JSON.stringify(message));
    }

    function getCurrentState(){
        return fetch(`http://${BACKEND_URL}/api/state/`, {method: 'GET'})
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response
        })
        .then(data => {
            return data;
        })
        .catch(error => {
            console.error(error);
            throw error;
        });
    }


    function getAllRecordings(){
        return fetch(`http://${BACKEND_URL}/api/recordings`, {method: 'GET'})
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(data => {
                return data;
            })
            .catch(error => {
                console.error(error);
                throw error;
            });
    }

    function deleteRecording(recordingId: number | null) {
        return fetch(`http://${BACKEND_URL}/api/recordings/${recordingId}`, {method: 'DELETE'})
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            return data;
        })
        .catch(error => {
            console.error(error);
            throw error;
        });
    }

    function deleteRecordings(recordingIds : [number]) {

        for (let i of recordingIds) {
            deleteRecording(i).then().catch(error => console.error(error));
        }
    }

    function getPlayForever(){
        return fetch(`http://${BACKEND_URL}/api/state/playing_forever`, {method: 'GET'})
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            return data;
        })
        .catch(error => {
            console.error(error);
            throw error;
        });
    }

    function setPlayForever(recording_ids:[number], play_forever:boolean, 
                            successCallback?: (response: any) => void, 
                            errorCallback?: (error: any) => void) {

        let update = {'recording_ids':recording_ids, 'play_forever':play_forever}
        fetch(`http://${BACKEND_URL}/api/state/playing_forever`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json', 
          },
          body: JSON.stringify(update), 
        })
          .then(response => {
            if (!response.ok) {
              throw new Error(`Request failed with status ${response.status}`);
            }
            return response.json();
          })
          .then(data => { if (successCallback) { successCallback(data) } } )
          .catch(error => {
            console.error(error);
            if (errorCallback) {
              errorCallback(error); 
            }
          });
      }


    return {
        record,
        play,
        playForever,
        stop,
        initialState,
        getAllRecordings,
        getCurrentState,
        deleteRecordings,
        getPlayForever,
        setPlayForever
    }

}