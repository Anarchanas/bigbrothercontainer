import type { StringTypeAnnotation } from '@babel/types';
import { getCurrentInstance } from 'vue';


const BACKEND_URL: string = import.meta.env.VITE_APP_BACKEND_URL;

interface SwitchCommand {
    command: string
    mqtt_id: string
    switch?: boolean
}

export function useSwitchCommander() {
    const vm = getCurrentInstance();

    if (!vm) {
      throw new Error("This function can only be used within `setup()`");
    }
    const socket = vm.appContext.config.globalProperties.$socket;


    function turnOn(mqtt_id:string) {
        let command = {command: "switch", mqtt_id: mqtt_id, switch: true}
        sendSwitchCommand(command);
    }

    function turnOff(mqtt_id:string) {
        let command = {command: "switch", mqtt_id: mqtt_id, switch: false}
        sendSwitchCommand(command);
    }

    function toggle(mqtt_id:string) {
        let command = {command: "toggle", mqtt_id: mqtt_id}
        sendSwitchCommand(command);
    }

    function sendSwitchCommand(cmd: SwitchCommand) {
        let request = {'type':'switch', 'data':cmd}
        socket.send(JSON.stringify(request))
    }

    function updateSwitch(mqtt_id: string, switchUpdate: any, successCallback?: (response: any) => void, errorCallback?: (error: any) => void) {
        fetch(`http://${BACKEND_URL}/api/switches/${mqtt_id}`, {
            method: 'PUT',
            headers: {
              'Content-Type': 'application/json', 
            },
            body: JSON.stringify(switchUpdate), 
          })
            .then(response => {
              if (!response.ok) {
                throw new Error(`Request failed with status ${response.status}`);
              }
              return response.json();
            })
            .then(data => { if (successCallback) { successCallback(data) } } )
            .catch(error => {
              console.error(error);
              if (errorCallback) {
                errorCallback(error); 
              }
            });
    }


    return {  
        turnOff,
        turnOn, 
        toggle,
        updateSwitch
      };

}