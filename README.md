# Bigbrother Container

Boris Nikitin produced the theatre "First Season. 20 Jahre Grosser Bruder" at the Staatstheater Nürnberg and had the original container from the TV production "Big Brother" reconstructed. [The Last Reality Show](https://borisnikitin.ch/en/artistic-works/the-last-reality-show) displayed the container in the [Tinguely Museum](https://www.tinguely.ch/en/exhibitions/exhibitions/2023/nikitin.html) open for visitors. 

This repository contains the code required to orchestrate the lights, shutters, music and switches within the container. 

![Shutters going up and down](/docs/images/timelapse_l.gif)

## Overview

The container contains 13 shutters, 30 lights and 3 switches. Each of these devices can be controlled in realtime through a webpage and every action performed can be recorded to replay them eternally during an exhibition. Additionally, this software allwos music tracks can be played to enhance the atmosphere within the exhibition.

To get an idea of its functionality, click on the gif for the full video. 

[![Dirigent: IoT for Exhibitions](/docs/images/demo_l.gif)](https://vimeo.com/921498257)

All components used within this project are open source.

### Components

![Software architecture schema](/docs/images/architecture_schema.png)


## Features

### Music

- Loop music track during playback
- Fade out / in when loop ends / starts
- Adjust volume (statically)

### Shutters

- Open / Close / Stop
- Adjust velocity dynamically
- Adjust per device default velocity
- Wait time until shutters are fully closed
- Initial State (opened, closed)

### Lights

- Turn on / off
- Dim warmth
- Dim brightness
- Trigger [WLED Presets](https://kno.wled.ge/features/presets/) (Blinking etc.)
- Fade within warmth
- Fade within brightness
- Initial State (brightness, warmth)

### Switches

- Turn on / off
- Initial State (on, off)

### Recording

- Recording and Playback of the before mentioned features
- Play once or forever in a loop
- Play multiple recordings at the same time


## Art

More about the art project itself can be found here:

- [Tinguely Museum's official publication](https://www.tinguely.ch/en/exhibitions/exhibitions/2023/nikitin.html)
- [Boris Nikitin's official publication](https://borisnikitin.ch/en/artistic-works/the-last-reality-show)
- [BZ Basel](https://www.bzbasel.ch/kultur/basel/museum-tinguely-boris-nikitin-damals-haben-wir-alle-ueber-den-big-brother-container-gelacht-heute-sitzen-wir-selbst-drin-ld.2558026?reduced=true)
- [Phosphor](https://phosphor-kultur.ch/kunst/boris-nikitin-the-last-reality-show)
