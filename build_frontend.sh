#!/bin/bash

# Usual one

cd frontend && cp .env .env.bak && npm install && npm run build && cd .. && rm -r dirigent/dist && cp -r frontend/dist dirigent/dist
cd frontend && cp .env.remote .env && npm install && npm run build && cp .env.bak .env && cd .. && rm -r dirigent/dist_remote && cp -r frontend/dist dirigent/dist_remote
