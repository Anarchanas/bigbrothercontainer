#!/bin/ash
# https://github.com/eclipse/mosquitto/blob/master/docker/2.0/docker-entrypoint.sh
set -e

# Set permissions
user="$(id -u)"
if [ "$user" = '0' ]; then
	[ -d "/mosquitto" ] && chown -R mosquitto:mosquitto /mosquitto || true
fi

if [ ! -f /mosquitto/data/password.txt ]; then
    mosquitto_passwd -b -c /mosquitto/data/password.txt mosquitto $MQTT_PASSWORD
    echo "created PW"
fi


/usr/sbin/mosquitto -c /mosquitto/config/mosquitto.conf