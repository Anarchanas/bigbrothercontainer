from fastapi import APIRouter, Depends, HTTPException
from api.devices.switches.schemas import SwitchBase, SwitchUpdate
from api.dependencies import WebSocketManager, get_ws_manager, get_db, get_mqtt_client
from api.crud import CRUDService
from logging import getLogger
import aiofiles
from api.audio.service import AudioPlayerService
from api.audio.schemas import ForegroundSongs, BackgroundSong
import os
from fastapi import FastAPI, File, UploadFile

# from ..dependencies import get_token_header
log = getLogger('dirigent')

mqtt_client = get_mqtt_client()

router = APIRouter(
    prefix="/api/audioplayer",
    tags=["audio"],
    responses={404: {"description": "Not found"}},
)


@router.put('/songs')
async def upload_song(file: UploadFile):
    file_location = f"./audio_files/{file.filename}"
    async with aiofiles.open(file_location, 'wb') as out_file:
        # Read async chunks of 1024 bytes
        while content := await file.read(1024):
            await out_file.write(content)  # Write async to disk

    return {"info": f"file '{file.filename}' saved at '{file_location}'"}


@router.get('/songs')
def list_songs():
    return os.listdir('./audio_files')


@router.get('/foreground')
async def get_foreground_songs(db=Depends(get_db)):
    volume = await CRUDService.get_config(db, 'foreground_volume')
    if volume is not None:
        volume = float(volume)
    songs = await CRUDService.get_config(db, 'foreground_songs')
    if songs is not None:
        songs = songs.split(",")
        songs = [s.split('/')[-1] for s in songs]

    return ForegroundSongs(filenames=sorted(songs), volume=volume)


@router.post('/foreground')
async def set_foreground_songs(config: ForegroundSongs, db=Depends(get_db)):
    available_songs = os.listdir('./audio_files')
    for song in config.filenames:
        if song not in available_songs:
            return HTTPException(501, f"Song is not available: {song}")
    songs = ['./audio_files/' + s for s in config.filenames]
    songs = ",".join(songs)
    volume = str(config.volume)
    await CRUDService.set_config(db, 'foreground_volume', volume)
    await CRUDService.set_config(db, 'foreground_songs', songs)
    db.flush()
    await AudioPlayerService.setup_foreground_channel(db)
    return


@router.delete('/foreground')
async def stop_foreground():
    AudioPlayerService.stop_foreground()
    return


@router.get('/background')
async def get_background_song(db=Depends(get_db)):
    volume = await CRUDService.get_config(db, 'background_volume')
    if volume is not None:
        volume = float(volume)
    song = await CRUDService.get_config(db, 'background_song')

    if song is not None:
        song = song.split('/')[-1]  # get rid of ./audio_files/
        return BackgroundSong(filename=song, volume=volume)
    else:
        return HTTPException(501, f"Not found {song}")


@router.post('/background')
async def set_background_song(config: BackgroundSong, db=Depends(get_db)):

    available_songs = os.listdir('./audio_files')
    if config.filename not in available_songs:
        return HTTPException(501, f"Song is not available: {config.filename}")

    song = './audio_files/' + config.filename
    volume = str(config.volume)
    await CRUDService.set_config(db, 'background_volume', volume)
    await CRUDService.set_config(db, 'background_song', song)
    db.flush()
    AudioPlayerService.stop_background()
    await AudioPlayerService.setup_background_channel(db)
    AudioPlayerService.background_channel.play()
    return


@router.delete('/background')
async def stop_background():
    AudioPlayerService.stop_background()
    return


@router.post('/volume')
async def set_volume(percentage: int):
    """ Adjust master volume in percent, defaults to 100 at application startup """
    AudioPlayerService.set_master_volume(percentage)
