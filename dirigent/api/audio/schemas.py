from pydantic import BaseModel, validator
from typing import Literal, Optional, List
from api.devices.schemas import Command, DeviceBase, DeviceUpdate


class BackgroundSong(BaseModel):
    filename: Optional[str]
    volume: Optional[float]


class ForegroundSongs(BaseModel):
    filenames: Optional[List[str]]
    volume: Optional[float]
