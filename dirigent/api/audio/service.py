import logging
from pydub import AudioSegment
from pydub.playback import _play_with_simpleaudio
import subprocess
from api.crud import CRUDService
from sqlalchemy.orm import Session
from apscheduler.schedulers.asyncio import AsyncIOScheduler
import datetime

log = logging.getLogger('dirigent')
scheduler = AsyncIOScheduler()
scheduler.start()


class AudioChannel():
    def __init__(self, playlist: [str], volume: float, loop: bool = False):
        self.playlist = sorted(playlist)
        self.playlist_idx = 0
        self.volume = volume
        self.playback = None
        self.data = None
        self.loop = loop
        self.load_next()
        self.job = None

    def play(self):
        if not self.loop:
            self.stop()
        if self.loop:
            if not scheduler.running:
                scheduler.start()
            again_in = datetime.timedelta(seconds=(len(self.data) / 1000) - 2)
            scheduled_at = datetime.datetime.now() + again_in
            self.job = scheduler.add_job(
                self.play, 'date', run_date=scheduled_at)
        self.playback = _play_with_simpleaudio(self.data)
        log.debug(
            f"Audio: playing {self.playlist[(self.playlist_idx-1) % len(self.playlist)]}")

    def is_playing(self):
        return self.playback is None or self.playback.is_playing()

    def stop(self):
        if self.playback is not None and self.is_playing:
            self.playback.stop()
            log.debug(
                f"Audio: stopped {self.playlist[(self.playlist_idx-1) % len(self.playlist)]}")
        if self.loop and self.job is not None:
            try:
                self.job.remove()
            except Exception as e:
                pass

    def load_next(self):
        filename = self.playlist[self.playlist_idx]
        self.data = AudioSegment.from_file(filename) + self.volume
        if self.loop:
            self.data = self.data.fade_in(2000).fade_out(2000)
        self.playlist_idx = (self.playlist_idx + 1) % len(self.playlist)


class AudioPlayerService:
    foreground_channel = None
    background_channel = None

    @staticmethod
    async def setup_foreground_channel(db: Session):
        playlist_songs = await CRUDService.get_config(db, "foreground_songs")
        if playlist_songs is not None:
            playlist = playlist_songs.split(",")
        else:
            log.error("No foreground songs chosen in DB")
            return None

        volume = await CRUDService.get_config(db, "foreground_volume")
        if volume is not None:
            volume = float(volume)
        else:
            log.error("No foreground volume chosen in DB")
            return None
        AudioPlayerService.foreground_channel = AudioChannel(playlist, volume)
        return AudioPlayerService.foreground_channel

    @staticmethod
    async def setup_background_channel(db: Session):
        background_song = await CRUDService.get_config(db, "background_song")
        if background_song is None:
            log.error("No background song chosen in DB")
            return None

        volume = await CRUDService.get_config(db, "background_volume")
        if volume is None:
            log.error("No background volume chosen in DB")
            return None
        volume = float(volume)
        AudioPlayerService.background_channel = AudioChannel(
            [background_song], volume, loop=True)
        return AudioPlayerService.background_channel

    @staticmethod
    async def setup(db: Session):
        log.info("Audio: reading audio files...")
        await AudioPlayerService.setup_background_channel(db)
        await AudioPlayerService.setup_foreground_channel(db)
        log.info("Audio: DONE reading audio files.")

    @staticmethod
    def set_master_volume(percentage):
        command = ["amixer", "set", "Master", "--", f"{percentage}%"]
        subprocess.run(command)

    @staticmethod
    def stop_background():
        if AudioPlayerService.background_channel is not None and AudioPlayerService.background_channel.is_playing():
            AudioPlayerService.background_channel.stop()

    @staticmethod
    def stop_foreground():
        if AudioPlayerService.foreground_channel is not None and AudioPlayerService.foreground_channel.is_playing():
            AudioPlayerService.foreground_channel.stop()

    @staticmethod
    async def handle_play():
        if AudioPlayerService.foreground_channel is not None:
            AudioPlayerService.foreground_channel.play()
        if AudioPlayerService.background_channel is not None and not AudioPlayerService.background_channel.is_playing():
            AudioPlayerService.background_channel.play()

    @staticmethod
    async def handle_play_forever():
        await AudioPlayerService.handle_play()
        AudioPlayerService.foreground_channel.load_next()
