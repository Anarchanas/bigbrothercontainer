import asyncio
import datetime
import logging
from fastapi import WebSocket
from sqlalchemy.orm import Session
from api.devices.schemas import DeviceBase
from api.devices.motors.schemas import MotorBase
from api.devices.lights.schemas import LightBase
from api.devices.models import Device, Motor, Light, Switch
from api.devices.motors.service import MotorService
from api.schemas import SocketRequest
from api.crud import CRUDService
from api.devices.switches.schemas import SwitchBase
import numpy as np
from api.config import device_config
from api.devices.lights.service import LightService
from api.devices.switches.service import SwitchService
from api.dependencies import get_ws_manager

from api.schemas import ErrorBase
from gmqtt import Client as MQTTClient

from api.dependencies import WebSocketManager

log = logging.getLogger('dirigent')
ws_manager = get_ws_manager()


class DeviceService:

    @staticmethod
    async def create(db: Session, device: DeviceBase) -> DeviceBase | None:
        """ Returns the newly created device or None if it was already existing"""
        if device.type == 'light':
            light = LightBase(**device.__dict__)
            return await CRUDService.create(db, Light, light)
        elif device.type == 'motor':
            motor = MotorBase(**device.__dict__)
            return await CRUDService.create(db, Motor, motor)
        elif device.type == 'switch':
            switch = SwitchBase(**device.__dict__)
            return await CRUDService.create(db, Switch, switch)

    @staticmethod
    async def register(db: Session, mqtt_client: MQTTClient, mqtt_id: str, device_type: str):
        """ Attempts to create a new device or else updates its last_seen timestamp """
        already_connected = False
        device_schema: DeviceBase = DeviceBase(
            type=device_type, mqtt_id=mqtt_id)
        device_schema.connected = True
        device_schema = await DeviceService.create(db, device_schema)

        if device_schema is None:
            device_schema = await CRUDService.get(db, Device, mqtt_id)
            already_connected = device_schema.connected
            device_schema.last_seen = datetime.datetime.now()
            device_schema.connected = True
            device_schema = await CRUDService.update(db, Device, device_schema, device_schema.mqtt_id)

        if device_type == 'motor':
            await MotorService.register(db, mqtt_client, device_schema.mqtt_id)
        elif device_type == 'light' and not already_connected:
            await LightService.register(db, mqtt_client, device_schema.mqtt_id)
        elif device_type == 'switch':
            await SwitchService.register(db, mqtt_client, device_schema.mqtt_id)

        if already_connected:
            return None
        return device_schema

    @staticmethod
    async def handle_websocket_command(db: Session, mqtt_client: MQTTClient, request: SocketRequest):
        """ Handles incoming websocket commands and performs generic error checks. 
        If successful, sends them to their appropriate services"""
        command = request.data
        device = await CRUDService.get(db, Device, command.mqtt_id)
        if device is None:
            return ErrorBase(error='DeviceService.handle_websocket_command', message=f'Device not found {command.mqtt_id}')
        if not device.connected:
            return ErrorBase(error='DeviceService.handle_websocket_command', message=f'Device offline {command.mqtt_id}')

        if request.type == 'motor' and device.type == 'motor':
            return await MotorService.handle_websocket_command(db, mqtt_client, command)
        elif request.type == 'light' and device.type == 'light':
            light = await LightService.handle_websocket_command(db, mqtt_client, command)
            if light.transition > 20:
                await ws_manager.broadcast(light)
            return light
        elif request.type == 'switch' and device.type == 'switch':
            return await SwitchService.handle_websocket_command(db, mqtt_client, command)
        return ErrorBase(error='DeviceService.handle_websocket_command', message=f"{request} went real bad")

    @staticmethod
    async def deregister(db: Session, mqtt_client: MQTTClient, mqtt_id: str, device_type: str):
        device_schema = await CRUDService.get(db, Device, mqtt_id)
        if device_schema is None:
            device_schema = await DeviceService.register(db, mqtt_client, mqtt_id, device_type)
        device_schema.connected = False
        device_schema = await CRUDService.update(db, Device, device_schema, device_schema.mqtt_id)
        return device_schema

    @staticmethod
    async def initial_state(db: Session, mqtt_client: MQTTClient):
        log.info("Entering Initial State....")
        online_devices = await DeviceService.who_is_online(db, mqtt_client)
        log.info(f"{len(online_devices)} devices online")
        tasks = []
        for device in online_devices:
            if device.type == 'motor':
                tasks.append(MotorService.get_to_initial_state(
                    db, mqtt_client, device))
            if device.type == 'light':
                tasks.append(LightService.get_to_initial_state(
                    db, mqtt_client, device))
            elif device.type == 'switch':
                tasks.append(SwitchService.get_to_initial_state(
                    db, mqtt_client, device))

        in_initial_state = await asyncio.gather(*tasks)
        in_initial_state = np.array(in_initial_state, dtype=int).sum()
        log.info(f"{in_initial_state} devices entered Initial State")
        return in_initial_state

    @staticmethod
    async def who_is_online(db: Session, mqtt_client: MQTTClient):
        async def online_attempt():
            DeviceService.whosthere(mqtt_client)
            await asyncio.sleep(device_config.online_timeout)
            devices = await CRUDService.get_all(db, Device)
            return np.array([d.connected for d in devices]).all()

        # SHOULD WE SET ALL DEVICES OFFLINE BEFORE QUERYING???
        all_online = await online_attempt()
        i = 1
        while not all_online and i < device_config.online_attempts:
            all_online = await online_attempt()
            i += 1

        devices = await CRUDService.get_all(db, Device)
        online_devices = []
        for i in range(len(devices)):
            if not devices[i].connected:
                log.error(
                    f"{devices[i].type} {devices[i].mqtt_id} is offline.")
            else:
                online_devices.append(devices[i])

        return online_devices

    @staticmethod
    def whosthere(mqtt_client: MQTTClient):
        mqtt_client.publish('whosthere', '', qos=1)

    @staticmethod
    async def push_all_devices(db: Session, ws_manager: WebSocketManager, websocket: WebSocket):
        devices = await CRUDService.get_all(db, Device)
        for d in devices:
            await ws_manager.send_personal_message(d, websocket)

    @staticmethod
    async def broadcast_all_devices(db: Session, ws_manager: WebSocketManager):
        devices = await CRUDService.get_all(db, Device)
        for d in devices:
            await ws_manager.broadcast(d)
