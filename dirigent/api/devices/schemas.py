""" Holds Device API schemas """
import datetime
from typing import Literal, Optional
from pydantic import BaseModel


class DeviceBase(BaseModel):
    """ Used for communication between server and webclient """
    type: Literal['motor', 'light', 'switch']
    name: str = None
    description: str = None
    mqtt_id: str
    connected: bool = False
    last_seen: datetime.datetime = datetime.datetime.now()
    group: str = ""
    tag: str = ""

    class Config:
        orm_mode = True


class DeviceUpdate(BaseModel):
    """ This is what a client submits to alter the entry in the DB"""
    name: Optional[str] = None
    description: Optional[str] = None
    group: Optional[str] = None
    tag: Optional[str] = None


class Command(BaseModel):
    """ Base class for websocket commands coming from webclient """
    command: str
