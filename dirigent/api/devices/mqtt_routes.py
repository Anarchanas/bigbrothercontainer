from api.dependencies import get_mqtt_fastapi, get_db, get_ws_manager
from api.schemas import ErrorBase
from api.devices.service import DeviceService
import logging
from api.crud import CRUDService
from api.devices.models import Light

mqtt_fastapi = get_mqtt_fastapi()
db = get_db().__next__()
ws_manager = get_ws_manager()
log = logging.getLogger('dirigent')


@mqtt_fastapi.on_connect()
def connect(client, flags, rc, properties):
    global mqtt_client
    if rc == 0:
        mqtt_client = client
        log.info(
            f"Mqtt with id {client._client_id} Connected to {client._host}:{client._port}")
    else:
        log.error(f"Problem when connecting MQTT client, error code {rc}")


@mqtt_fastapi.subscribe("register/+")
async def register_device(client, topic, payload, qos, properties):
    """ Used to register switches and motors """
    mqtt_id = payload.decode('utf-8')
    device_type = topic.split("/")[-1]
    device = await DeviceService.register(db, client, mqtt_id, device_type)
    if device is not None:
        await ws_manager.broadcast(device)


@mqtt_fastapi.subscribe("wled/+/status")
async def register_light(client, topic, payload, qos, properties):
    """ Registering of WLEDS """
    status = payload.decode('utf-8')
    mqtt_id = topic.split("/")[1]

    if status == 'online':
        device = await DeviceService.register(db, client, mqtt_id, 'light')
    elif status == 'offline':
        # Ugly but because of retain msgs we make this hack to first create
        # the device within the db and then deregister it...
        device = await DeviceService.deregister(db, client, mqtt_id, 'light')

    # The WLEDS register themselves quite often. It is questionable if the lights should
    # be broadcasted each time... Alternative is a poll mechanism from frontend.
    if device is not None:
        await ws_manager.broadcast(device)


@mqtt_fastapi.subscribe("deregister")
async def deregister_device(client, topic, payload, qos, properties):
    """ Marks device as offline within DB """
    mqtt_id = payload.decode('utf-8')
    device = await DeviceService.deregister(db, client, mqtt_id, 'motor')
    await ws_manager.broadcast(device)


@mqtt_fastapi.on_disconnect()
def mqtt_disconnect(client, packet, exc=None):
    """ Just log a message when main client loses connection. There is not much we can do... """
    log.critical("MQTT client disconnected")
