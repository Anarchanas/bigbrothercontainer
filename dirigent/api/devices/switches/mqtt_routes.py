from api.dependencies import get_mqtt_fastapi, get_db, get_ws_manager
from fastapi import APIRouter
from api.schemas import ErrorBase
from api.devices.switches.service import SwitchService
import logging

mqtt_fastapi = get_mqtt_fastapi()
db = get_db().__next__()
ws_manager = get_ws_manager()
log = logging.getLogger('dirigent')


@mqtt_fastapi.subscribe("switch/+/on")
async def listen_to_on_switch(client, topic, payload, qos, properties):
    device_type, mqtt_id, state = topic.split('/')
    switch = await SwitchService.update_state(db, mqtt_id, state)
    await ws_manager.broadcast(switch)


@mqtt_fastapi.subscribe("switch/+/off")
async def listen_to_off_switch(client, topic, payload, qos, properties):
    device_type, mqtt_id, state = topic.split('/')
    switch = await SwitchService.update_state(db, mqtt_id, state)
    await ws_manager.broadcast(switch)
