import asyncio
import logging
from api.crud import CRUDService
from api.devices.models import Switch
from api.devices.switches.schemas import SwitchBase, SwitchCommand
from gmqtt import Client as MQTTClient


log = logging.getLogger('dirigent')


class SwitchService:

    @staticmethod
    async def register(db, mqtt_client, mqtt_id: str):
        pass

    @staticmethod
    async def handle_websocket_command(db, mqtt_client: MQTTClient, command: SwitchCommand):
        switch = await CRUDService.get(db, Switch, command.mqtt_id)
        if command.command == 'toggle':
            mqtt_client.publish(f'switch/{command.mqtt_id}/toggle')
        elif command.command == 'switch':
            if command.switch:
                mqtt_client.publish(f'switch/{command.mqtt_id}/turn_on')
            else:
                mqtt_client.publish(f'switch/{command.mqtt_id}/turn_off')
        return switch

    @staticmethod
    async def get_to_initial_state(db, mqtt_client: MQTTClient, switch: SwitchBase):
        tries = 3
        for i in range(tries):
            cmd = SwitchCommand(
                command='switch', switch=switch.initial_turned_on, mqtt_id=switch.mqtt_id)
            await SwitchService.handle_websocket_command(db, mqtt_client, cmd)
            await asyncio.sleep(0.5)

        return True

    @staticmethod
    async def turn_off(mqtt_client: MQTTClient, mqtt_id):
        await mqtt_client.publish(f"switch/{mqtt_id}/turn_on", None, qos=1)

    @staticmethod
    async def turn_on(mqtt_client: MQTTClient, mqtt_id):
        await mqtt_client.publish(f"switch/{mqtt_id}/turn_off", None, qos=1)

    @staticmethod
    async def toggle(mqtt_client: MQTTClient, mqtt_id):
        await mqtt_client.publish(f"switch/{mqtt_id}/toggle", None, qos=1)

    @staticmethod
    async def update_state(db, mqtt_id, state):
        switch: SwitchBase = await CRUDService.get(db, Switch, mqtt_id)
        if state == 'on':
            if not switch.turned_on:
                switch.turned_on = True
                return await CRUDService.update(db, Switch, switch, mqtt_id)
        elif state == 'off':
            if switch.turned_on:
                switch.turned_on = False
                return await CRUDService.update(db, Switch, switch, mqtt_id)
        else:
            log.error(f'Unknown state: {state}')
            return
        return switch
