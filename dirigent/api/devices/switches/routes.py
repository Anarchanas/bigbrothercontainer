import asyncio
from fastapi import APIRouter, Depends, HTTPException
from api.devices.switches.schemas import SwitchBase, SwitchUpdate
from api.dependencies import WebSocketManager, get_ws_manager, get_db, get_mqtt_client
from api.crud import CRUDService
from logging import getLogger
from api.devices.models import Switch
from fastapi import BackgroundTasks
# from ..dependencies import get_token_header
log = getLogger('dirigent')

mqtt_client = get_mqtt_client()

router = APIRouter(
    prefix="/api/switches",
    tags=["switches"],
    responses={404: {"description": "Not found"}},
)


@router.put('/{mqtt_id}')
async def update_switch(mqtt_id: str, switch_update: SwitchUpdate, db=Depends(get_db)):
    switch = await CRUDService.update(db, Switch, switch_update, mqtt_id)
    if switch is None:
        return HTTPException(404)
    else:
        return switch


@router.get('/')
async def get_all_lights(db=Depends(get_db)):
    switches = await CRUDService.get_all(db, Switch)
    if switches is None:
        return HTTPException(404)
    else:
        return switches
