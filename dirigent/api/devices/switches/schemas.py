from pydantic import BaseModel, validator
from typing import Literal, Optional
from api.devices.schemas import Command, DeviceBase, DeviceUpdate


class SwitchBase(DeviceBase):
    type: Literal['switch'] = 'switch'
    turned_on: bool = False
    initial_turned_on: bool = False


class SwitchUpdate(DeviceUpdate):
    turned_on: Optional[bool] = None
    initial_turned_on: Optional[bool] = None


class SwitchCommand(Command):
    command: Literal['toggle', 'switch'] = 'switch'
    switch: Optional[bool] = None
    mqtt_id: str
