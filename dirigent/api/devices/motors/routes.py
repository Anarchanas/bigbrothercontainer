import asyncio
from fastapi import APIRouter, Depends, HTTPException
from api.devices.motors.schemas import MotorCommand, MotorUpdate
from api.devices.service import DeviceService
from api.devices.motors.service import MotorService
from api.dependencies import WebSocketManager, get_ws_manager, get_db, get_mqtt_client
from logging import getLogger
from fastapi import BackgroundTasks
from api.crud import CRUDService
from api.devices.models import Motor

# from ..dependencies import get_token_header
log = getLogger('dirigent')

mqtt_client = get_mqtt_client()

router = APIRouter(
    prefix="/api/motors",
    tags=["motors"],
    responses={404: {"description": "Not found"}},
)


@router.put('/{mqtt_id}')
async def update_motor(mqtt_id: str, motorUpdate: MotorUpdate, db=Depends(get_db)):
    motor = await CRUDService.update(db, Motor, motorUpdate, mqtt_id)
    if motor is None:
        return HTTPException(404)
    else:
        return motor


@router.get('/')
async def get_all_motors(db=Depends(get_db)):
    motors = await CRUDService.get_all(db, Motor)
    if motors is None:
        return HTTPException(404)
    else:
        return motors


@router.post("/hit_bottom_timeout/{timeout}", status_code=204)
async def set_all_hit_bottom_timeout(timeout: int, db=Depends(get_db), mqtt_client=Depends(get_mqtt_client)):
    devices = await MotorService.get_all_online(db)
    for d in devices:
        await MotorService.set_hit_bottom_timeout(
            db, mqtt_client, d.mqtt_id, timeout)
    return


@router.post("/{mqtt_id}/hit_bottom_timeout/{timeout}", status_code=204)
async def set_hit_bottom_timeout(mqtt_id: str, timeout: int, db=Depends(get_db)):
    motor = await CRUDService.get(db, Motor, mqtt_id)
    if motor is None:
        return HTTPException(404, detail=f"Not found {mqtt_id}")
    await MotorService.set_hit_bottom_timeout(db, mqtt_client, motor, timeout)
    return
