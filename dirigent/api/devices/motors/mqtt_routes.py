from api.dependencies import get_mqtt_fastapi, get_db, get_ws_manager
from fastapi import APIRouter
from api.schemas import ErrorBase
from api.devices.motors.service import MotorService
import logging

mqtt_fastapi = get_mqtt_fastapi()
db = get_db().__next__()
ws_manager = get_ws_manager()
log = logging.getLogger('dirigent')


@mqtt_fastapi.subscribe("motor/+/opening")
async def message_to_topic(client, topic, payload, qos, properties):
    motor = await MotorService.handle_mqtt_message(db, topic, payload)
    await ws_manager.broadcast(motor)


@mqtt_fastapi.subscribe("motor/+/closing")
async def update_closing(client, topic, payload, qos, properties):
    motor = await MotorService.handle_mqtt_message(db, topic, payload)
    await ws_manager.broadcast(motor)


@mqtt_fastapi.subscribe("motor/+/hit_bottom")
async def update_hit_bottom(client, topic, payload, qos, properties):
    motor = await MotorService.handle_mqtt_message(db, topic, payload)
    await ws_manager.broadcast(motor)


@mqtt_fastapi.subscribe("motor/+/hit_top")
async def update_hit_top(client, topic, payload, qos, properties):
    motor = await MotorService.handle_mqtt_message(db, topic, payload)
    await ws_manager.broadcast(motor)


@mqtt_fastapi.subscribe("motor/+/stopped")
async def update_stopped(client, topic, payload, qos, properties):
    motor = await MotorService.handle_mqtt_message(db, topic, payload)
    await ws_manager.broadcast(motor)


@mqtt_fastapi.subscribe("motor/+/bottom_timeout")
async def update_bottom_timeout(client, topic, payload, qos, properties):
    await MotorService.handle_mqtt_message(db, topic, payload)
