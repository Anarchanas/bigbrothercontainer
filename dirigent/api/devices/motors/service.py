import asyncio
import logging
import struct
from sqlalchemy.orm import Session
from api.devices.motors.schemas import MotorBase, MotorUpdate
from api.schemas import SocketResponse, ErrorBase
from gmqtt import Client as MQTTClient
from api.devices.models import Motor
from api.devices.motors.schemas import MotorBase, MotorCommand
from api.crud import CRUDService
from api.config import motor_config

log = logging.getLogger('dirigent')


class MotorService:

    @staticmethod
    async def handle_websocket_command(db: Session, mqtt_client: MQTTClient, command: MotorCommand):
        if command.command == 'close':
            return await MotorService.close(db, mqtt_client, command)
        elif command.command == 'open':
            return await MotorService.open(db, mqtt_client, command)
        elif command.command == 'stop':
            return await MotorService.stop(db, mqtt_client, command)
        else:
            return ErrorBase(error='MotorService.handle_command', message=f'Unknown command {command.command}')

    @staticmethod
    async def get_all_online(db: Session):
        devices = db.query(Motor).all()
        [db.refresh(d) for d in devices]
        return [device.as_schema() for device in devices if device.connected]

    @staticmethod
    async def close(db: Session,
                    mqtt_client: MQTTClient,
                    command: MotorCommand
                    ) -> SocketResponse:
        motor_db = db.get(Motor, command.mqtt_id)
        if motor_db is None:
            error = ErrorBase(
                error='MotorService.close', message=f"Device with mqtt_id {command.mqtt_id} was not found")
            return error
        motor_schema = MotorBase(**motor_db.__dict__)
        velocity = struct.pack('I', command.velocity)  # Unsigned int
        mqtt_client.publish(
            f"motor/{motor_schema.mqtt_id}/close", payload=velocity, qos=1)
        return motor_schema

    @staticmethod
    async def open(db: Session,
                   mqtt_client: MQTTClient,
                   command: MotorCommand
                   ) -> SocketResponse:

        motor_db = db.get(Motor, command.mqtt_id)
        if motor_db is None:
            error = ErrorBase(
                error='MotorService.open', message=f"Device with mqtt_id {command.mqtt_id} was not found")
            return error

        motor_schema = MotorBase(**motor_db.__dict__)
        velocity = struct.pack('I', command.velocity)  # Unsigned int
        mqtt_client.publish(
            f"motor/{motor_schema.mqtt_id}/open", payload=velocity, qos=1)

        return motor_schema

    @staticmethod
    async def stop(db: Session,
                   mqtt_client: MQTTClient,
                   command: MotorCommand
                   ) -> SocketResponse:
        motor_db = db.get(Motor, command.mqtt_id)
        if motor_db is None:
            error = ErrorBase(
                error='MotorService.stop', message=f"Device with mqtt_id {command.mqtt_id} was not found")
            return error
        motor_schema = MotorBase(**motor_db.__dict__)
        velocity = struct.pack('I', 0)  # Unsigned int
        mqtt_client.publish(
            f"motor/{motor_schema.mqtt_id}/stop", payload=velocity, qos=1)
        return motor_schema

    @staticmethod
    async def get_to_initial_state(db: Session, mqtt_client: MQTTClient, motor: MotorBase):
        timer = 0
        if motor.initial_position == 'on_bottom':
            while not motor.on_bottom and timer < motor_config.initial_state_timeout:
                if not motor.closing:
                    await MotorService.close(db, mqtt_client, MotorCommand(
                        command='close', mqtt_id=motor.mqtt_id, velocity=motor.default_v_closing))
                await asyncio.sleep(0.5)
                timer += 0.5
                motor = await CRUDService.get(db, Motor, motor.mqtt_id)

            # Either we reached init state or timeout..
            if not motor.on_bottom:
                log.error(
                    f"{motor.mqtt_id} could not reach initial state within {motor_config.initial_state_timeout}s")
            await CRUDService.update(db, Motor, motor, motor.mqtt_id)

            return motor.on_bottom

        elif motor.initial_position == 'on_top':
            while not motor.on_top and timer < motor_config.initial_state_timeout:
                if not motor.opening:
                    await MotorService.open(db, mqtt_client, MotorCommand(
                        command='open', mqtt_id=motor.mqtt_id, velocity=motor.default_v_opening))
                await asyncio.sleep(0.5)
                timer += 0.5
                motor = await CRUDService.get(db, Motor, motor.mqtt_id)
            if not motor.on_top:
                log.error(
                    f"{motor.mqtt_id} could not reach initial state within {motor_config.initial_state_timeout}s.")
            await CRUDService.update(db, Motor, motor, motor.mqtt_id)
            return motor.on_top

    @staticmethod
    async def set_hit_bottom_timeout(db, mqtt_client, motor_schema: MotorBase, timeout):
        """ The shades hit the bottom sensor. However it needs more closing. This sets the time 
        of how long it keeps closing until it emits the on_bottom event """

        motor_update = MotorUpdate(**motor_schema.dict())
        motor_update.bottom_timeout = int(timeout)
        await CRUDService.update(db, Motor, motor_update, motor_schema.mqtt_id)
        timeout = struct.pack('I', timeout)  # Unsigned int
        mqtt_client.publish(
            f"motor/{motor_schema.mqtt_id}/set_bottom_timeout", payload=timeout, qos=1)
        return timeout

    @staticmethod
    async def register(db, mqtt_client, mqtt_id):
        """ Immediatly sets the specified timeout whenever a device is online """
        motor_schema = await CRUDService.get(db, Motor, mqtt_id)
        if motor_schema is None:
            error = ErrorBase(
                error='MotorService.register', message=f"Device with mqtt_id {mqtt_id} was not found")
            return error

        await MotorService.set_hit_bottom_timeout(
            db, mqtt_client, motor_schema, motor_schema.bottom_timeout)

    @staticmethod
    async def handle_mqtt_message(db: Session,
                                  topic: str,
                                  payload=''):

        log.debug(f"mqtt <-: {topic}, {payload}")
        topic = topic.split("/")
        type, mqtt_id, command = topic[0], topic[1], topic[2]

        if type != "motor":
            log.error(f"Unknown topic: {topic}")
            return ErrorBase(error="MotorService.handle_mqtt_message", message=f"Unknown mqtt topic: {topic}")

        motor_schema = await CRUDService.get(db, Motor, mqtt_id)
        if motor_schema is None:
            error_msg = f"Received Message from an unregistered device: {mqtt_id}"
            log.error(error_msg)
            return ErrorBase(error="MotorService.handle_mqtt_message", message=error_msg)

        if command == 'hit_top':
            motor_schema.on_top = True
            motor_schema.on_bottom = False
            motor_schema.opening = False
            motor_schema.closing = False
            motor_schema.velocity = int(payload)
        elif command == 'hit_bottom':
            motor_schema.on_top = False
            motor_schema.on_bottom = True
            motor_schema.opening = False
            motor_schema.closing = False
            motor_schema.velocity = int(payload)
        elif command == 'stopped':
            motor_schema.on_top = False
            motor_schema.closing = False
            motor_schema.opening = False
            motor_schema.on_bottom = False
            motor_schema.velocity = int(payload)
        elif command == 'opening':
            motor_schema.opening = True
            motor_schema.closing = False
            motor_schema.on_bottom = False
            motor_schema.on_top = False
            motor_schema.velocity = int(payload)
        elif command == 'closing':
            motor_schema.closing = True
            motor_schema.opening = False
            motor_schema.on_bottom = False
            motor_schema.on_top = False
            motor_schema.velocity = int(payload)
        elif command == 'bottom_timeout':
            motor_schema.bottom_timeout = int(payload)

        else:
            error_msg = f"Unknown command type for mqtt message {command}"
            log.error(error_msg)
            return ErrorBase(error="MotorService.handle_mqtt_message", message=error_msg)

        motor_schema = await CRUDService.update(db, Motor, motor_schema, motor_schema.mqtt_id)
        return motor_schema
