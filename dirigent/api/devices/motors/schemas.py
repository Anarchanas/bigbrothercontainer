from pydantic import BaseModel, validator
from typing import Literal, Optional
from api.devices.schemas import DeviceBase, Command, DeviceUpdate


class MotorBase(DeviceBase):
    type: Literal['motor'] = 'motor'
    closing: bool = False
    opening: bool = False
    on_top: bool = False
    on_bottom: bool = False
    velocity: int = 0
    bottom_timeout: int = 0
    initial_position: Literal['on_top', 'on_bottom'] = 'on_top'
    default_v_closing: int = 255
    default_v_opening: int = 255

    @validator('velocity')
    def min_max(cls, v):
        if v < 0 or v > 255:
            raise ValueError('must be between 0 and 255')
        return v

    class Config:
        orm_mode = True


class MotorUpdate(DeviceUpdate):
    bottom_timeout: Optional[int] = None
    initial_position: Literal['on_top', 'on_bottom'] = 'on_top'
    default_v_closing: Optional[int] = None
    default_v_opening: Optional[int] = None


class MotorCommand(Command):
    """ From Webclient to motor """
    command: Literal["open", "close", "stop"]
    mqtt_id: str
    velocity: int = 0

    class Config:
        orm_mode = True
