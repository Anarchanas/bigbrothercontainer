from api.devices.lights.schemas import LightBase
from api.devices.switches.schemas import SwitchBase
from api.database import Base
from api.devices.schemas import DeviceBase
from sqlalchemy.orm import Mapped, mapped_column
import datetime
from api.devices.motors.schemas import MotorBase


class Device(Base):
    __tablename__ = "devices"

    mqtt_id: Mapped[str] = mapped_column(primary_key=True)
    type: Mapped[str]
    name: Mapped[str] = mapped_column(nullable=True)
    description: Mapped[str] = mapped_column(nullable=True)
    connected: Mapped[bool]
    last_seen: Mapped[datetime.datetime]
    group: Mapped[str] = mapped_column(nullable=True)
    tag: Mapped[str] = mapped_column(nullable=True)

    __mapper_args__ = {
        "polymorphic_on": "type",
        "polymorphic_identity": "device"
    }

    def update(self, changes: DeviceBase):
        for key, val in changes.__dict__.items():
            if val is not None:
                setattr(self, key, val)
        return self


class Motor(Device):

    closing: Mapped[bool] = mapped_column(nullable=True)
    opening: Mapped[bool] = mapped_column(nullable=True)
    on_top: Mapped[bool] = mapped_column(nullable=True)
    on_bottom: Mapped[bool] = mapped_column(nullable=True)
    velocity: Mapped[int] = mapped_column(nullable=True)
    bottom_timeout: Mapped[int] = mapped_column(nullable=True)
    initial_position: Mapped[str] = mapped_column(nullable=True)
    default_v_opening: Mapped[int] = mapped_column(nullable=True)
    default_v_closing: Mapped[int] = mapped_column(nullable=True)

    __mapper_args__ = {
        "polymorphic_identity": "motor"
    }

    def update(self, changes: MotorBase):
        for key, val in changes.__dict__.items():
            if val is not None:
                setattr(self, key, val)
        return self

    def as_schema(self):
        return MotorBase(**self.__dict__)


class Light(Device):

    brightness: Mapped[int] = mapped_column(nullable=True)
    temperature: Mapped[int] = mapped_column(nullable=True)
    preset: Mapped[int] = mapped_column(nullable=True)
    transition: Mapped[int] = mapped_column(nullable=True)
    initial_brightness: Mapped[int] = mapped_column(nullable=True)
    initial_temperature: Mapped[int] = mapped_column(nullable=True)
    initial_preset: Mapped[int] = mapped_column(nullable=True)
    initial_transition: Mapped[int] = mapped_column(nullable=True)
    turned_on: Mapped[bool] = mapped_column(nullable=True)
    default_special_preset: Mapped[int] = mapped_column(nullable=True)
    default_preset: Mapped[int] = mapped_column(nullable=True)

    __mapper_args__ = {
        "polymorphic_identity": "light"
    }

    def update(self, changes: LightBase):
        for key, val in changes.__dict__.items():
            if val is not None:
                setattr(self, key, val)
        return self

    def as_schema(self):
        return LightBase(**self.__dict__)


class Switch(Device):

    turned_on: Mapped[bool] = mapped_column(
        nullable=True, use_existing_column=True)
    initial_turned_on: Mapped[bool] = mapped_column(nullable=True)

    __mapper_args__ = {
        "polymorphic_identity": "switch"
    }

    def update(self, changes: SwitchBase):
        for key, val in changes.__dict__.items():
            if val is not None:
                setattr(self, key, val)
        return self

    def as_schema(self):
        return SwitchBase(**self.__dict__)
