from fastapi import APIRouter, Depends, HTTPException
from .service import DeviceService
from api.dependencies import WebSocketManager, get_ws_manager, get_db, get_mqtt_client
from api.crud import CRUDService
from logging import getLogger
from api.devices.models import Device, Motor, Light
from api.devices.schemas import DeviceUpdate

log = getLogger('dirigent')

mqtt_client = get_mqtt_client()

router = APIRouter(
    prefix="/api/devices",
    tags=["devices"],
    responses={404: {"description": "Not found"}},
)


@router.get("/", status_code=204)
async def device_get(db=Depends(get_db), mqtt_client=Depends(get_mqtt_client), ws_manager: WebSocketManager = Depends(get_ws_manager)):
    DeviceService.whosthere(mqtt_client)
    devices = await CRUDService.get_all(db, Device)
    for d in devices:
        await ws_manager.broadcast(d)
    return


@router.delete("/", status_code=204)
async def device_get(db=Depends(get_db)):
    await CRUDService.delete_all(db, Device)
    return


@router.put('/{mqtt_id}')
async def update_device(mqtt_id: str, device_update: DeviceUpdate, db=Depends(get_db), ws_manager: WebSocketManager = Depends(get_ws_manager)):
    device = await CRUDService.update(db, Device, device_update, mqtt_id)
    if device is None:
        return HTTPException(404)
    else:
        return device


@router.delete('/{mqtt_id}')
async def update_device(mqtt_id: str, db=Depends(get_db)):
    await CRUDService.delete(db, Device, mqtt_id)
    return


@router.get("/whosthere", status_code=204)
async def whosthere(mqtt_client=Depends(get_mqtt_client)):
    DeviceService.whosthere(mqtt_client)
