from fastapi import APIRouter, Depends, HTTPException
from api.devices.lights.schemas import LightUpdate
from api.devices.models import Light
from api.dependencies import get_db, get_mqtt_client
from logging import getLogger
from api.crud import CRUDService
from api.devices.lights.service import LightService
from api.devices.lights.schemas import WledCommand

# from ..dependencies import get_token_header
log = getLogger('dirigent')

mqtt_client = get_mqtt_client()

router = APIRouter(
    prefix="/api/lights",
    tags=["lights"],
    responses={404: {"description": "Not found"}},
)


@router.put('/{mqtt_id}')
async def update_light(mqtt_id: str, light_update: LightUpdate, db=Depends(get_db)):
    light = await CRUDService.update(db, Light, light_update, mqtt_id)

    if light is None:
        return HTTPException(404)
    else:
        return light


@router.get('/')
async def get_all_lights(db=Depends(get_db)):
    lights = await CRUDService.get_all(db, Light)
    if lights is None:
        return HTTPException(404)
    else:
        return lights
