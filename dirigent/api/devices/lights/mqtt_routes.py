from api.dependencies import get_mqtt_fastapi, get_db, get_ws_manager
from fastapi import APIRouter
from api.schemas import ErrorBase
from api.devices.service import DeviceService, MotorService
import logging

mqtt_fastapi = get_mqtt_fastapi()
db = get_db().__next__()
ws_manager = get_ws_manager()
log = logging.getLogger('dirigent')
