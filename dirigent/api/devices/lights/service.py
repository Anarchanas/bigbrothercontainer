import asyncio
import logging
from sqlalchemy.orm import Session
from gmqtt.client import Client as MQTTClient
from api.devices.lights.schemas import LightCommand, LightUpdate, LightBase
from api.devices.lights.schemas import WledCommand, convert_light_to_wled, exclude_optional_json
from api.crud import CRUDService
from api.devices.models import Light

log = logging.getLogger('dirigent')


class LightService:

    @staticmethod
    async def handle_websocket_command(db: Session, mqtt_client: MQTTClient, command: LightCommand):
        light: Light = await CRUDService.get(db, Light, command.mqtt_id)

        db_update = LightUpdate(**command.__dict__)

        wled_cmd: WledCommand = convert_light_to_wled(command)

        if wled_cmd.on is None:
            wled_cmd.on = light.turned_on

        if command.group is not None:
            LightService.publish_wled_api(
                mqtt_client, command.group, wled_cmd)
        else:
            LightService.publish_wled_api(
                mqtt_client, command.mqtt_id, wled_cmd)

        light = await CRUDService.update(db, Light, db_update, command.mqtt_id)

        return light

    @staticmethod
    async def register(db, mqtt_client, mqtt_id):
        """ Main registering process is within DeviceService, this is just
        for light specific onboarding process """
        light: Light = await CRUDService.get(db, Light, mqtt_id)

        # Send transition settings to the light
        cmd = WledCommand(transition=light.transition)
        LightService.publish_wled_api(mqtt_client, mqtt_id, cmd)
        return

    @staticmethod
    async def get_to_initial_state(db: Session, mqtt_client: MQTTClient, light: LightBase):
        tries = 3
        for i in range(tries):

            # .. temperature,
            cmd = LightCommand(mqtt_id=light.mqtt_id,
                               brightness=light.initial_brightness,
                               temperature=light.initial_temperature,
                               transition=light.initial_transition
                               )
            await asyncio.sleep(0.1)
            await LightService.handle_websocket_command(db, mqtt_client, cmd)

            # .. and preset differently due to wled API
            cmd = LightCommand(command='preset',
                               mqtt_id=light.mqtt_id,
                               preset=light.initial_preset,
                               )
            await asyncio.sleep(0.1)
            await LightService.handle_websocket_command(db, mqtt_client, cmd)

            # .. and preset differently due to wled API
            cmd = LightCommand(command='on',
                               mqtt_id=light.mqtt_id,
                               turned_on=True,
                               )
            await asyncio.sleep(0.1)
            await LightService.handle_websocket_command(db, mqtt_client, cmd)

            await asyncio.sleep(1)

        return True

    @staticmethod
    def publish_wled_api(mqtt_client: MQTTClient, mqtt_id_group: str, cmd: WledCommand):
        """ Both individual devices and groups can be adressed """
        payload = exclude_optional_json(cmd)
        mqtt_client.publish(f'wled/{mqtt_id_group}/api', payload, qos=1)
        return
