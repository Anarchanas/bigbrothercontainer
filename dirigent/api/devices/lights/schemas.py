import json
from typing import Literal, Optional, List
from api.devices.schemas import Command, DeviceBase, DeviceUpdate
from pydantic import BaseModel, validator
from pydantic.json import pydantic_encoder


class LightBase(DeviceBase):
    type: Literal['light'] = 'light'
    brightness: int = 1
    temperature: int = 1
    preset: Optional[int] = 1
    transition: int = 1
    initial_brightness: int = 1
    initial_temperature: int = 1
    initial_preset: int = 1
    initial_transition: int = 1
    turned_on: bool = True
    default_special_preset: int = 2
    default_preset: int = 1

    class Config:
        orm_mode = True

    @classmethod
    @validator('temperature', 'brightness', 'initial_brightness', 'initial_temperature')
    def min_max(cls, v):
        if v < 0 or v > 255:
            raise ValueError('must be between 0 and 255')
        return v


class LightUpdate(DeviceUpdate):
    brightness: Optional[int] = None
    temperature: Optional[int] = None
    preset: Optional[int] = None
    transition: Optional[int] = None
    initial_brightness: Optional[int] = None
    initial_temperature: Optional[int] = None
    initial_preset: Optional[int] = None
    initial_transition: Optional[int] = None
    turned_on: Optional[bool] = None
    default_special_preset: Optional[int] = None
    default_preset: Optional[int] = None

    @classmethod
    @validator('temperature', 'brightness', 'initial_brightness', 'initial_temperature')
    def min_max(cls, v):
        if v is None:
            return v
        if v < 0 or v > 255:
            raise ValueError('must be between 0 and 255')
        return v


class LightCommand(Command):
    """ From Webclient to light """
    command: Literal["static", "preset", "on"] = 'static'
    mqtt_id: str
    brightness: Optional[int]
    temperature: Optional[int]
    preset: Optional[int]
    group: Optional[str]
    turned_on: Optional[bool]
    transition: Optional[int]

    class Config:
        orm_mode = True

    @validator('temperature', 'brightness')
    @classmethod
    def min_max(cls, v):
        if v is None:
            return v

        if v < 0 or v > 255:
            raise ValueError('must be between 0 and 255')
        return v


class WledCommand(BaseModel):
    bri: Optional[int] = None
    cct: Optional[int] = None
    ps: Optional[int] = None
    on: Optional[bool] = None
    transition: Optional[int] = None
    seg: Optional[List] = None


# https://stackoverflow.com/questions/65362524/in-json-created-from-a-pydantic-basemodel-exclude-optional-if-not-set
def exclude_optional_dict(model: BaseModel):
    return {**model.dict(exclude_none=True)}


def exclude_optional_json(model: BaseModel):
    return json.dumps(exclude_optional_dict(model), default=pydantic_encoder)


def convert_light_to_wled(light_cmd: LightCommand) -> WledCommand:
    if light_cmd.turned_on is not None:
        return WledCommand(on=light_cmd.turned_on)
    if light_cmd.preset is not None:
        if light_cmd.transition is not None:
            return WledCommand(ps=light_cmd.preset, transition=light_cmd.transition)
        return WledCommand(ps=light_cmd.preset)
    cct = None
    if light_cmd.temperature is not None:
        cct = WledCommand(cct=light_cmd.temperature)
        cmd = WledCommand(bri=light_cmd.brightness,
                          transition=light_cmd.transition,
                          seg=[cct])
    else:
        cmd = WledCommand(bri=light_cmd.brightness,
                          transition=light_cmd.transition,)
    return cmd
