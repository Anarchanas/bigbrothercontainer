import dotenv
import os
from pydantic import BaseModel, BaseSettings
import logging


BBCONTAINER_ENV_FILE = os.getenv('BBCONTAINER_ENV_FILE')
if BBCONTAINER_ENV_FILE is None:
    dotenv.load_dotenv(dotenv_path='../.env', verbose=True)
    print("LOADED .env")
else:
    dotenv.load_dotenv(dotenv_path=f'../{BBCONTAINER_ENV_FILE}', verbose=True)
    print(f"LOADED {BBCONTAINER_ENV_FILE}")

PLAYING_FOREVER = os.getenv('DIRIGENT_PLAYING_ID')
LOG_LEVEL = os.getenv('DIRIGENT_LOG_LEVEL')

if PLAYING_FOREVER is not None:
    print('FOREVER LOOP STARTUP is set')

print(f"Set Log Level to {LOG_LEVEL}")


log_handlers = ['default']
if os.getenv('DIRIGENT_ENVIRONMENT') == 'PROD':
    log_handlers.append("rotationLogger")
    print("Added Log File Handler")


log_config = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "default": {
            "()": "uvicorn.logging.DefaultFormatter",
            "fmt": "%(levelprefix)s %(asctime)s %(filename)s:%(lineno)d %(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S",

        },
    },
    "handlers": {
        "default": {
            "formatter": "default",
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stderr",
        },
        "rotationLogger": {
            "formatter": "default",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": "./dirigent.log",
            "mode": "a",
            "maxBytes": 10*1024*1024,
            "backupCount": 3,
            "encoding": "utf-8"
        }
    },
    "loggers": {
        "dirigent": {"handlers": log_handlers, "level": LOG_LEVEL}
    },
}

db_config = {
    "db_file": os.getenv('DB_FILE')
}


mqtt_config = {
    "username": os.getenv('MQTT_USER'),
    "password": os.getenv('MQTT_PASSWORD'),
    "host": os.getenv('MQTT_ADDRESS'),
    "port": int(os.getenv('MQTT_PORT')),
    "reconnect_retries": -1,  # -1 try it forever
    "reconnect_delay": 6,
}


class DeviceConfig(BaseSettings):
    online_timeout: int = 0.5
    online_attempts: int = 5


class MotorConfig(DeviceConfig):
    initial_state_timeout = 60*5


class ScheduleTimes(BaseSettings):
    start_hour = 11
    start_minute = 00
    stop_hour = 20
    stop_minute = 50
    misfire_grace_time = 60*5


device_config = DeviceConfig()
motor_config = MotorConfig()
schedule_times = ScheduleTimes()
