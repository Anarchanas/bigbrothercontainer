from pydantic import BaseModel
from typing import Literal, List, Optional


class ApplicationState(BaseModel):
    state: Literal[
        'PLAYING_FOREVER',
        'RECORDING',
        'PLAYING_ONCE',
        'FREESTYLE',
        'INITIALISING',
    ] = 'FREESTYLE'


class LoopUpdate(ApplicationState):
    type: Literal['loop'] = 'loop'


class LoopCommand(BaseModel):
    type: Literal['loop'] = 'loop'
    command: Literal['record',
                     'play',
                     'play_forever',
                     'stop',
                     'initial_state']
    name: str = None
    recording_ids: List[int] = None


class ApplicationConfigBase(BaseModel):
    config_key: Literal['playing_forever_ids',
                        'play_forever',
                        'background_song',
                        'background_volume',
                        'foreground_songs',
                        'foreground_volume']
    config_value: str


class ForeverRecordingsUpdate(BaseModel):
    play_forever: Optional[bool] = None
    recording_ids: Optional[List[int]] = None
