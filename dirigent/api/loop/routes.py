from fastapi import APIRouter, Depends
from api.dependencies import get_ws_manager, get_db
from api.loop.service import LoopService
from api.loop.schemas import LoopUpdate
from sqlalchemy.orm import Session
from api.loop.schemas import ForeverRecordingsUpdate
from api.crud import CRUDService
from api.loop.models import ApplicationConfig
import logging

log = logging.getLogger('dirigent')

router = APIRouter(
    prefix="/api/state",
    tags=["state"],
    responses={404: {"description": "Not found"}},
)


@router.get("/")
async def get_all_recordings(ws_manager=Depends(get_ws_manager)):
    await ws_manager.broadcast(LoopUpdate(
        type='loop', state=LoopService.state.state))
    return


@router.get("/playing_forever")
async def get_forever_recordings(db=Depends(get_db)):
    play_forever = await CRUDService.get_config(db, "play_forever")
    recording_ids = await CRUDService.get_config(db, "playing_forever_ids")
    if recording_ids is not None:
        recording_ids = [int(i) for i in recording_ids.split(",")]
    play_forever = play_forever == '1'
    return ForeverRecordingsUpdate(play_forever=play_forever, recording_ids=recording_ids)


@router.put("/playing_forever", status_code=200)
async def set_forever_recordings(update: ForeverRecordingsUpdate, db=Depends(get_db)):

    if update.play_forever is not None and update.play_forever:
        config = await CRUDService.set_config(db, 'play_forever', '1')
    elif update.play_forever is not None and not update.play_forever:
        config = await CRUDService.set_config(db, 'play_forever', '0')

    if update.recording_ids is not None:
        recording_ids = ",".join([str(u) for u in update.recording_ids])
        await CRUDService.set_config(db, 'playing_forever_ids', recording_ids)
    log.info(update)
    return update
