
from api.config import PLAYING_FOREVER, schedule_times
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from api.loop.schemas import ApplicationState
from api.devices.service import DeviceService
from api.recorder.service import PlayService, RecorderService
import datetime
from api.dependencies import WebSocketManager
from sqlalchemy.orm import Session
from api.loop.schemas import LoopCommand, LoopUpdate
from gmqtt import Client as MQTTClient
from api.schemas import ErrorBase
import logging
from api.loop.models import ApplicationConfig
from api.loop.schemas import ApplicationConfigBase
from api.crud import CRUDService
from api.audio.service import AudioPlayerService


"""
Basically a huge state machine
"""

log = logging.getLogger('dirigent')


class LoopService:
    state = ApplicationState(state='FREESTYLE')
    scheduler = AsyncIOScheduler()
    forever_recording_ids = PLAYING_FOREVER
    recording_ids = []
    playing_forever_job = None
    start_play_job = None
    stop_play_job = None

    @staticmethod
    async def entrypoint(db: Session, mqtt_client: MQTTClient, ws_manager: WebSocketManager):
        await AudioPlayerService.setup(db)
        AudioPlayerService.set_master_volume(100)
        if AudioPlayerService.background_channel is not None:
            AudioPlayerService.background_channel.play()

        play_forever = await CRUDService.get_config(db, "play_forever")
        if play_forever is None:
            play_forever = await CRUDService.set_config(db, "play_forever", "0")

        if not LoopService.scheduler.running:
            LoopService.scheduler.start()
        if play_forever == '0':
            LoopService.state = ApplicationState(state='FREESTYLE')
            log.info("Entered Freestyle mode")

        elif play_forever == '1':
            recording_ids = await CRUDService.get_config(db, "playing_forever_ids")
            LoopService.recording_ids = [int(r)
                                         for r in recording_ids.split(",")]

            await LoopService.play_forever(db, mqtt_client, ws_manager)
            # await LoopService.schedule_start_end(db, mqtt_client, ws_manager)
        else:
            log.error(f"Unknown setting for play_forever {play_forever}")

    @staticmethod
    async def schedule_start_end(db: Session, mqtt_client: MQTTClient, ws_manager: WebSocketManager):
        for i in range(10):
            f = LoopService.handle_play_forever
            args = (db, mqtt_client, ws_manager, LoopService.recording_ids)
            log.info(
                f"Scheduled play forever at {schedule_times.start_hour + i}:{schedule_times.start_minute:02d}")
            LoopService.playing_forever_job = LoopService.scheduler.add_job(
                f, 'cron', args,
                hour=schedule_times.start_hour + i,
                minute=schedule_times.start_minute,
                misfire_grace_time=schedule_times.misfire_grace_time,
                timezone='Europe/Zurich'
            )

        f = LoopService.handle_stop
        args = (db, ws_manager)
        LoopService.scheduler.add_job(
            f, 'cron', args,
            hour=schedule_times.start_hour - 4,
            minute=50,
            misfire_grace_time=schedule_times.misfire_grace_time,
            timezone='Europe/Zurich')

        for i in range(5):
            f = LoopService.handle_initial_state
            args = (db, mqtt_client, ws_manager)
            LoopService.scheduler.add_job(
                f, 'cron', args,
                hour=schedule_times.start_hour - 1,
                minute=30 + 5*i,
                misfire_grace_time=schedule_times.misfire_grace_time,
                timezone='Europe/Zurich')

        log.info("Scheduled jobs to play forever...")

    @staticmethod
    async def play_forever(db: Session, mqtt_client: MQTTClient, ws_manager: WebSocketManager):
        LoopService.state = ApplicationState(state='PLAYING_FOREVER')
        online_devices = await DeviceService.who_is_online(db, mqtt_client)
        if len(online_devices) > 0:
            await LoopService.initial_state(db, mqtt_client, ws_manager)
        else:
            # No one online, try again
            timeout = 60
            log.info(f"No one online.. Trying again in {timeout} seconds")
            f = LoopService.play_forever
            again_in = datetime.timedelta(seconds=timeout)
            scheduled_at = datetime.datetime.now() + again_in
            args = (db, mqtt_client, ws_manager)
            LoopService.playing_forever_job = LoopService.scheduler.add_job(
                f, 'date', args, run_date=scheduled_at)

    @staticmethod
    async def initial_state(db: Session, mqtt_client: MQTTClient, ws_manager: WebSocketManager, command: LoopCommand = None):
        # Notify User about changed state
        log.info(LoopService.state.state)
        former_state = LoopService.state.state
        LoopService.state.state = "INITIALISING"
        await ws_manager.broadcast(LoopUpdate(state=LoopService.state.state))

        # Actually go for it
        n_devices = await DeviceService.initial_state(db, mqtt_client)
        await DeviceService.broadcast_all_devices(db, ws_manager)

        if former_state == 'PLAYING_FOREVER':
            LoopService.state.state = "PLAYING_FOREVER"
            await ws_manager.broadcast(LoopUpdate(state=LoopService.state.state))
            # If nobody ready try again...
            if n_devices <= 0:
                await LoopService.play_forever(db, mqtt_client, ws_manager)
                return LoopUpdate(state=LoopService.state.state)

            playing = await PlayService.play(db,
                                             mqtt_client,
                                             recording_ids=LoopService.recording_ids,
                                             final_f=LoopService.play_forever,
                                             final_args=(
                                                 db, mqtt_client, ws_manager)
                                             )
            await AudioPlayerService.handle_play_forever()
            if not playing:
                return await LoopService.handle_stop(db, ws_manager)
            return LoopUpdate(state=LoopService.state.state)

        if former_state == "FREESTYLE":
            LoopService.state.state = "FREESTYLE"
            return LoopUpdate(state=LoopService.state.state)

        if former_state == 'RECORDING':
            LoopService.state.state = "RECORDING"
            RecorderService.start_record(db, command.name)
            if command.recording_ids is not None and len(command.recording_ids) > 0:
                await PlayService.play(db,
                                       mqtt_client,
                                       recording_ids=LoopService.recording_ids,
                                       final_f=None,
                                       final_args=None
                                       )
            await AudioPlayerService.handle_play()
            return LoopUpdate(state=LoopService.state.state)

        if former_state == 'PLAYING_ONCE':
            LoopService.state.state = "PLAYING_ONCE"
            await PlayService.play(db,
                                   mqtt_client,
                                   recording_ids=LoopService.recording_ids,
                                   final_f=LoopService.handle_stop,
                                   final_args=(db, ws_manager)
                                   )
            await AudioPlayerService.handle_play()
            return LoopUpdate(state=LoopService.state.state)

    @staticmethod
    async def handle_play(db: Session, mqtt_client: MQTTClient, ws_manager: WebSocketManager, recording_ids: [int]):
        if LoopService.state.state in ["PLAYING_ONCE", "PLAYING_FOREVER", "RECORDING", "INITIALISING"]:
            return ErrorBase(error='LoopService.handle_play', message=f'{LoopService.state.state} Stop first.')

        if LoopService.state.state == "FREESTYLE":
            LoopService.state.state = "PLAYING_ONCE"
            LoopService.recording_ids = recording_ids
            return await LoopService.initial_state(db, mqtt_client, ws_manager)

        return True

    @staticmethod
    async def handle_play_forever(db: Session, mqtt_client: MQTTClient, ws_manager: WebSocketManager, recording_ids: [int]):
        if LoopService.state.state in ["PLAYING_ONCE", "PLAYING_FOREVER", "RECORDING", "INITIALISING"]:
            return ErrorBase(error='LoopService.handle_play', message=f'{LoopService.state.state} Stop first.')

        if LoopService.state.state == "FREESTYLE":
            LoopService.state.state = "PLAYING_FOREVER"
            LoopService.recording_ids = recording_ids
            return await LoopService.initial_state(db, mqtt_client, ws_manager)

        return ErrorBase(error='LoopService.handle_play', message='WTF')

    @staticmethod
    async def handle_stop(db: Session, ws_manager: WebSocketManager):
        if LoopService.state.state in ["INITIALISING"]:
            return ErrorBase(error='LoopService.handle_stop', message='Cannot stop while initialising...')

        if LoopService.state.state in ["PLAYING_ONCE"]:
            await PlayService.stop()
            LoopService.state.state = "FREESTYLE"
            await ws_manager.broadcast(LoopUpdate(state=LoopService.state.state))

        if LoopService.state.state in ["PLAYING_FOREVER"]:
            await PlayService.stop()
            if LoopService.playing_forever_job is not None:
                if LoopService.playing_forever_job.next_run_time.timestamp() > datetime.datetime.now().timestamp():
                    LoopService.playing_forever_job.remove()
                LoopService.playing_forever_job = None

        if LoopService.state.state in ["RECORDING"]:
            await PlayService.stop()
            RecorderService.stop_record(db)

        AudioPlayerService.stop_foreground()
        LoopService.state.state = "FREESTYLE"
        log.info(f"Stopped, entering {LoopService.state.state}")
        return LoopUpdate(state=LoopService.state.state)

    @staticmethod
    async def handle_record(db: Session, mqtt_client: MQTTClient, ws_manager: WebSocketManager, command: LoopCommand):
        if LoopService.state.state in ["PLAYING_ONCE", "PLAYING_FOREVER", "RECORDING", "INITIALISING"]:
            return ErrorBase(error='LoopService.handle_record', message=f'{LoopService.state.state} Stop first.')

        if LoopService.state.state in ["FREESTYLE"]:
            LoopService.recording_ids = command.recording_ids
            LoopService.state.state = "RECORDING"
            return await LoopService.initial_state(db, mqtt_client, ws_manager, command)

        return ErrorBase(error='LoopService.handle_record', message='WTF')

    @staticmethod
    async def handle_initial_state(db: Session, mqtt_client: MQTTClient, ws_manager: WebSocketManager):
        if LoopService.state.state in ["FREESTYLE"]:
            return await LoopService.initial_state(db, mqtt_client, ws_manager)
        elif LoopService.state.state in ["RECORDING"]:
            # We just send the signals to go into the initial state without
            #         adjusting the application state.
            await DeviceService.initial_state(db, mqtt_client)
        else:
            return ErrorBase(error='LoopService.handle_initial_state', message=f'{LoopService.state.state} Stop first.')

    @staticmethod
    async def handle_websocket_command(db: Session, mqtt_client: MQTTClient, ws_manager: WebSocketManager, command: LoopCommand):
        if command.command == 'play':
            return await LoopService.handle_play(
                db, mqtt_client, ws_manager, command.recording_ids)
        elif command.command == "play_forever":
            return await LoopService.handle_play_forever(
                db, mqtt_client, ws_manager, command.recording_ids)
        elif command.command == 'record':
            return await LoopService.handle_record(db, mqtt_client, ws_manager, command)
        elif command.command == 'stop':
            return await LoopService.handle_stop(db, ws_manager)
        elif command.command == 'initial_state':
            return await LoopService.handle_initial_state(db, mqtt_client, ws_manager)
