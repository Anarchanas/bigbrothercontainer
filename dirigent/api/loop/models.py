
from sqlalchemy.orm import Mapped, mapped_column
from api.database import Base
from api.loop.schemas import ApplicationConfigBase


class ApplicationConfig(Base):
    __tablename__ = "application_config"

    id: Mapped[int] = mapped_column(index=True, primary_key=True)
    config_key: Mapped[str] = mapped_column(unique=True)
    config_value: Mapped[str] = mapped_column()

    def as_schema(self):
        return ApplicationConfigBase(**self.__dict__)
