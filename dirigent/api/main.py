from fastapi import FastAPI
from fastapi.responses import RedirectResponse
from fastapi.staticfiles import StaticFiles

import logging
from fastapi.middleware.cors import CORSMiddleware
from api.dependencies import get_db, get_ws_manager, get_mqtt_client, get_mqtt_fastapi
from api.devices.routes import router as device_router
from api.devices.motors.routes import router as motor_router
from api.recorder.routes import router as recorder_router
from api.websocket import router as websocket_router
from api.devices.switches.routes import router as switches_router
from api.audio.routes import router as audio_router
import api.devices.mqtt_routes
import api.devices.motors.mqtt_routes
import api.devices.switches.mqtt_routes
from api.loop.routes import router as loop_router
from api.loop.service import LoopService
import asyncio
from api.devices.lights.routes import router as light_router


app = FastAPI()

# CORS
origins = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Add routes
app.include_router(device_router)
app.add_websocket_route('/ws', websocket_router)
app.include_router(recorder_router)
app.include_router(motor_router)
app.include_router(loop_router)
app.include_router(light_router)
app.include_router(switches_router)
app.include_router(audio_router)


# Configure MQTT
mqtt_fastapi = get_mqtt_fastapi()
mqtt_fastapi.init_app(app)
mqtt_client = get_mqtt_client()


log = logging.getLogger('dirigent')

# DB
db = get_db().__next__()

# Websocket
ws_manager = get_ws_manager()

# Frontend
app.mount('/app', StaticFiles(directory='dist', html=True))

# This is just used when we make an ssh tunnel. See the frontend build bash file.
app.mount('/remote', StaticFiles(directory='dist_remote', html=True))


@app.on_event('startup')
async def start_loop():
    await LoopService.entrypoint(db, mqtt_client, ws_manager)


@app.get("/")
async def redirect_app():
    """ Routes to the frontend static path """
    return RedirectResponse('/app')
