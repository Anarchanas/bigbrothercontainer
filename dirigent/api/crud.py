

from sqlalchemy.orm import Session
from api.devices.models import Motor, Switch, Light
from sqlalchemy.future import select
from api.loop.models import ApplicationConfig
from api.loop.schemas import ApplicationConfigBase


class CRUDService:

    @staticmethod
    async def get_config(db: Session, config_key_name: str):
        config_value = db.query(ApplicationConfig).where(
            ApplicationConfig.config_key == config_key_name).first()

        if config_value is None:
            return config_value
        return config_value.config_value

    @staticmethod
    async def set_config(db: Session, config_key_name: str, config_new_value: str):
        config_value: ApplicationConfig = db.query(ApplicationConfig).where(
            ApplicationConfig.config_key == config_key_name).first()
        if config_value is None:
            config_value = ApplicationConfig(**ApplicationConfigBase(
                config_key=config_key_name, config_value=config_new_value).dict())
            db.add(config_value)
            db.commit()
            db.refresh(config_value)
        else:
            config_value.config_value = config_new_value
            db.commit()
            db.refresh(config_value)
        return config_value.as_schema().config_value

    @staticmethod
    async def get_all(db: Session, model):
        items = db.query(model).all()
        [db.refresh(m) for m in items]
        return [m.as_schema() for m in items]

    @staticmethod
    async def get(db: Session, model, id):
        item = db.get(model, id)
        if item is None:
            return None
        db.refresh(item)
        return item.as_schema()

    @staticmethod
    async def delete_all(db: Session, model):
        item = db.query(model).all()
        for d in item:
            db.delete(d)
            db.commit()
        db.flush()

    @staticmethod
    async def delete(db: Session, model, id):
        item = db.get(model, id)
        db.delete(item)
        db.commit()
        db.flush()

    @staticmethod
    async def update(db: Session, model, schema, id):
        item = db.get(model, id)

        if item is None:
            return None
        else:
            item = item.update(schema)
            db.commit()
            db.refresh(item)
            return item.as_schema()

    @staticmethod
    async def create(db: Session, model, schema):
        # Annoying hack because I used mqtt_id as the id for devices...
        if model in [Motor, Light, Switch]:
            item = db.get(model, schema.mqtt_id)
        else:
            item = db.get(model, schema.id)

        if item is None:
            item = model(**schema.__dict__)
            db.add(item)
            db.commit()
            db.refresh(item)
            return item.as_schema()
        else:
            return None
