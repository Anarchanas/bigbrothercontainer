from api.devices.schemas import DeviceBase
from api.devices.motors.schemas import MotorBase, MotorCommand
from api.devices.lights.schemas import LightBase, LightCommand
from api.devices.switches.schemas import SwitchBase, SwitchCommand
import datetime
from pydantic import BaseModel
from typing import Literal, Union
from api.loop.schemas import LoopCommand, LoopUpdate


class ErrorBase(BaseModel):
    type: Literal["error"] = "error"
    error: str
    message: str


class PollCommand(BaseModel):
    message: str


class SocketRequest(BaseModel):
    type: Literal['motor', 'light', 'device',
                  'recorder', 'poll', 'loop', 'switch']
    data: Union[MotorCommand, LightCommand,
                PollCommand, LoopCommand, SwitchCommand]


class PollResponse(BaseModel):
    message: str = datetime.datetime.utcnow()
    type: Literal['poll'] = 'poll'


class SocketResponse(BaseModel):
    type: Literal['motor', 'light', 'device', 'switch',
                  'recorder', 'error', 'poll', 'loop']
    data: Union[SwitchBase, MotorBase, LightBase, DeviceBase,
                ErrorBase, PollResponse, LoopUpdate]


def clientErrorResponse(error, message):
    response = ErrorBase(error=error, message=message)
    return SocketResponse(type='error', data=response)
