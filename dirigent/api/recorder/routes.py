from fastapi import APIRouter, Depends, HTTPException
from api.dependencies import get_ws_manager, get_db
from api.recorder.service import RecorderService
from api.recorder.service import Recording
from api.crud import CRUDService
# from ..dependencies import get_token_header

router = APIRouter(
    prefix="/api/recordings",
    tags=["recordings"],
    responses={404: {"description": "Not found"}},
)


@router.get("/")
async def get_all_recordings(db=Depends(get_db), ws_manager=Depends(get_ws_manager)):
    recordings = await CRUDService.get_all(db, Recording)
    return recordings


@router.get("/{recording_id}/records")
async def get_records(recording_id: int, db=Depends(get_db), ws_manager=Depends(get_ws_manager)):
    recordings = db.get(Recording, recording_id)
    if recordings is None:
        return HTTPException(404)
    return recordings.records


@router.delete("/{recording_id}")
async def delete_records(recording_id: int, db=Depends(get_db)):
    recording = db.get(Recording, recording_id)
    if recording is not None:
        await CRUDService.delete(db, Recording, recording_id)
    else:
        return HTTPException(404)
    return
