import datetime
import logging
from typing import List
from sqlalchemy.orm import Session
from api.devices.motors.schemas import MotorCommand
from api.devices.models import Device
from api.devices.motors.service import MotorService
from api.recorder import models, schemas
from gmqtt import Client as MQTTClient
from api.crud import CRUDService
from api.recorder.models import Recording, Record, MotorRecord, LightRecord
from api.devices.switches.schemas import SwitchCommand
import numpy as np
from apscheduler.jobstores.base import JobLookupError
from api.devices.lights.schemas import LightCommand
from api.devices.service import DeviceService


import time
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from api.schemas import clientErrorResponse, SocketRequest
from apscheduler.job import Job
log = logging.getLogger('dirigent')


class PlayService:
    recordings = []
    longest_recording = 0
    all_records = []
    scheduler = AsyncIOScheduler()
    jobs: [Job] = []

    @staticmethod
    async def play(db: Session, mqtt_client: MQTTClient, recording_ids: [int], final_f=None, final_args=None):

        if not await PlayService.gather_recordings(db, recording_ids):
            return False

        if not PlayService.scheduler.running:
            PlayService.scheduler.start()
        log.info(
            f"Playing {len(PlayService.recordings)} recordings, will take {PlayService.longest_recording} seconds")

        for record in PlayService.all_records:
            _record = db.get(Record, record.id)
            db.refresh(_record)
            if record.type == 'motor':
                cmd = MotorCommand(**_record.__dict__)
            elif record.type == 'light':
                cmd = LightCommand(**_record.__dict__)
            elif record.type == 'switch':
                cmd = SwitchCommand(**_record.__dict__)
            else:
                log.error(f"not implemented record to play: {record.type}")
                break

            cmd = SocketRequest(type=record.type, data=cmd)
            args = (db, mqtt_client, cmd)
            f = DeviceService.handle_websocket_command
            td = datetime.timedelta(seconds=record.offset)
            scheduled_at = datetime.datetime.now() + td
            PlayService.jobs.append(PlayService.scheduler.add_job(
                f, 'date', args, run_date=scheduled_at))

        # final_f should be either stop, none (if recording) or play_forever
        # It also could be stop if we say that recording is ending automatically.
        if final_f is not None:
            td = datetime.timedelta(seconds=PlayService.longest_recording + 1)
            scheduled_at = datetime.datetime.now() + td
            PlayService.jobs.append(PlayService.scheduler.add_job(
                final_f, 'date', final_args, name='cancel', run_date=scheduled_at))
        return True

    @staticmethod
    async def stop():
        for j in PlayService.jobs:
            if j.next_run_time.timestamp() > datetime.datetime.now().timestamp():
                j.remove()

        PlayService.jobs = []
        PlayService.recordings = []
        PlayService.all_records = []
        PlayService.longest_recording = 0

    @staticmethod
    async def gather_recordings(db: Session, recording_ids: [int]):
        await PlayService.stop()
        durations = []
        for id in recording_ids:
            recording = db.get(Recording, id)
            if recording is None:
                log.error(f"Not found recording with id {id}")
                return False

            db.refresh(recording)

            if recording is not None:
                PlayService.recordings.append(recording)
                durations.append(recording.duration)
                PlayService.all_records += recording.records

        if len(durations) > 0:
            PlayService.longest_recording = np.array(durations).max()
        else:
            PlayService.longest_recording = 0.2
        return True


class RecorderService:
    current_recording = None
    recording = False

    @staticmethod
    def start_record(session: Session, name: str = None) -> bool:
        log.info("Start Recording")
        db_recording = models.Recording(created=datetime.datetime.now(),
                                        is_recording=True,
                                        time=time.time(),
                                        duration=0, name=name)

        RecorderService.current_recording = db_recording
        session.add(db_recording)
        session.commit()
        session.refresh(RecorderService.current_recording)
        RecorderService.recording = True
        return True

    @staticmethod
    def stop_record(db: Session) -> bool:

        RecorderService.recording = False
        db.refresh(RecorderService.current_recording)

        RecorderService.current_recording.duration = time.time(
        ) - RecorderService.current_recording.time
        RecorderService.current_recording.is_recording = False
        db.commit()
        db.refresh(RecorderService.current_recording)
        RecorderService.current_recording = None
        log.info("Stop Recording")
        return True

    @staticmethod
    def record(db: Session, request: SocketRequest):
        if not RecorderService.recording:
            return

        command = request.data
        constructor = command.dict()

        constructor['type'] = request.type
        constructor['recording_id'] = RecorderService.current_recording.id
        constructor['offset'] = time.time(
        ) - RecorderService.current_recording.time
        if request.type == 'motor':
            record_schema = schemas.MotorRecordBase(**constructor)
            record = models.MotorRecord(**record_schema.__dict__)
        elif request.type == 'light':
            record_schema = schemas.LightRecordBase(**constructor)
            record = models.LightRecord(**record_schema.__dict__)
        elif request.type == 'switch':
            record_schema = schemas.SwitchRecordBase(**constructor)
            record = models.SwitchRecord(**record_schema.__dict__)
        else:
            log.error(f"Unknown type {request.type}")
            return

        db.add(record)
        db.commit()
        db.refresh(record)
        return record_schema
