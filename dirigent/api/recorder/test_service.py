
import pytest
import pytest
from api.stubs.motor_stub import MotorStub
from fastapi.testclient import TestClient
from fastapi import WebSocket
from api.test_fixtures import get_db, get_ws_manager
import httpx
import websockets
import json
import asyncio
from api.devices.motors.schemas import MotorCommand
from api.websocket import SocketRequest
from api.loop.schemas import LoopCommand

BACKEND_URL = 'http://localhost:8000'
WEBSOCKET_URL = 'ws://localhost:8000/ws'
MOTOR_MQTT_ID = 'test_1'


def socket_request(command, type):
    return SocketRequest(type=type, data=command).json()


async def get_json(queue: asyncio.Queue):
    await asyncio.sleep(0.3)
    msg = await queue.get()
    msg = json.loads(msg)
    msg = json.loads(msg)
    return msg


# async def run_stubs(timeout):
#     # Keep script running for an hour for demo purposes
#     stubs = []
#     for i in range(5):
#         stubs.append(MotorStub(f"stub_{i}", reactive=True))
#         await stubs[i].connect()

#     await asyncio.sleep(timeout)

#     for stub in stubs:
#         await stub.disconnect()


# def start_new_event_loop(coroutine):
#     new_loop = asyncio.new_event_loop()
#     asyncio.set_event_loop(new_loop)
#     new_loop.run_until_complete(coroutine)
#     new_loop.close()


@pytest.mark.asyncio
async def test_play():
    async with httpx.AsyncClient() as client:
        async with websockets.connect(WEBSOCKET_URL) as ws:
            # start_new_event_loop(run_stubs(10))
            cmd = LoopCommand(type='loop', command='record', name='record_1')

            await ws.send(socket_request(cmd, 'loop'))
            await asyncio.sleep(3)
            cmd = MotorCommand(type='motor', command='open',
                               velocity=244, mqtt_id='stub_0')
            await ws.send(socket_request(cmd, 'motor'))

            await asyncio.sleep(1)
            cmd = MotorCommand(type='motor', command='close',
                               velocity=244, mqtt_id='stub_0')
            await ws.send(socket_request(cmd, 'motor'))

            await asyncio.sleep(1)

            cmd = MotorCommand(type='motor', command='stop',
                               velocity=244, mqtt_id='stub_0')
            await ws.send(socket_request(cmd, 'motor'))

            cmd = LoopCommand(type='loop', command='stop', name='record_1')
            await ws.send(socket_request(cmd, 'loop'))

            response = await client.get(BACKEND_URL + "/api/recordings/")
            assert len(response.json()) == 1

            cmd = LoopCommand(type='loop', command='play', recording_ids=[1])
            await ws.send(socket_request(cmd, 'loop'))
            await asyncio.sleep(10)

            assert False
