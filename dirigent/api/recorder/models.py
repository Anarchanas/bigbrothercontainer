from typing import List
from api.database import Base
from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import relationship
import datetime
from api.recorder.schemas import RecordingBase, RecordBase


class Recording(Base):
    __tablename__ = "recordings"

    id: Mapped[int] = mapped_column(primary_key=True, index=True)
    description: Mapped[str] = mapped_column(nullable=True)
    name: Mapped[str] = mapped_column(nullable=True)
    created: Mapped[datetime.datetime]
    time: Mapped[float]
    is_recording: Mapped[bool]
    records: Mapped[List["Record"]] = relationship(
        back_populates="recording", cascade="all, delete-orphan")
    duration: Mapped[float]

    def as_schema(self):
        return RecordingBase(**self.__dict__)


class Record(Base):
    __tablename__ = "records"

    id: Mapped[int] = mapped_column(primary_key=True, index=True)
    offset: Mapped[float]
    type: Mapped[str]
    command: Mapped[str]
    mqtt_id: Mapped[str] = mapped_column(ForeignKey("devices.mqtt_id"))
    recording_id: Mapped[int] = mapped_column(ForeignKey("recordings.id"))
    recording: Mapped["Recording"] = relationship(back_populates="records")

    __mapper_args__ = {
        "polymorphic_on": "type",
        "polymorphic_identity": "base"
    }

    def as_schema(self):
        return RecordBase(**self.__dict__)


class MotorRecord(Record):
    velocity: Mapped[int] = mapped_column(nullable=True)

    __mapper_args__ = {
        "polymorphic_identity": "motor"
    }


class LightRecord(Record):
    temperature: Mapped[int] = mapped_column(nullable=True)
    brightness: Mapped[int] = mapped_column(nullable=True)
    preset: Mapped[int] = mapped_column(nullable=True)
    turned_on: Mapped[bool] = mapped_column(nullable=True)
    transition: Mapped[int] = mapped_column(nullable=True)
    group: Mapped[str] = mapped_column(nullable=True)

    __mapper_args__ = {
        "polymorphic_identity": "light"
    }


class SwitchRecord(Record):
    switch: Mapped[bool] = mapped_column(nullable=True)

    __mapper_args__ = {
        "polymorphic_identity": "switch"
    }
