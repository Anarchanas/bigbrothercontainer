from pydantic import BaseModel, ValidationError, validator

from typing import Literal, Optional, Union, List
import time
import datetime


class RecordBase(BaseModel):
    id: Optional[int] = None
    offset: float
    type: Literal["motor", "light", "switch"]
    command: str
    mqtt_id: str
    recording_id: int


class RecordingBase(BaseModel):
    """ For client """
    id: int
    description: Optional[str]
    name: Optional[str]
    created: datetime.datetime
    duration: float


class MotorRecordBase(RecordBase):
    velocity: int


class LightRecordBase(RecordBase):
    brightness: Optional[int]
    temperature: Optional[int]
    preset: Optional[int]
    group: Optional[str]
    turned_on: Optional[bool]
    transition: Optional[int]


class SwitchRecordBase(RecordBase):
    switch: Optional[bool] = None
