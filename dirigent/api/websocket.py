from fastapi import APIRouter, Depends, HTTPException, WebSocketDisconnect
from api.dependencies import get_db
from sqlalchemy.orm import Session
from fastapi import WebSocket
from api.devices.motors.service import MotorService
from api.recorder.service import RecorderService
from api.schemas import SocketRequest, PollResponse, ErrorBase
from pydantic import ValidationError
from api.loop.service import LoopService
from api.devices.service import DeviceService

import logging

from api.dependencies import get_mqtt_client, get_ws_manager


# Dependencies
log = logging.getLogger('dirigent')
db = get_db()
router = APIRouter()
ws_manager = get_ws_manager()


@router.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket, db: Session = Depends(get_db)):
    mqtt_client = get_mqtt_client()
    await ws_manager.connect(websocket)
    try:
        while True:
            data = await websocket.receive_json()

            log.debug(f"<--- {data}")
            try:
                response = None
                validated = SocketRequest.validate(data)

                if validated.type in ['motor', 'light', 'switch']:
                    response = await DeviceService.handle_websocket_command(
                        db, mqtt_client, validated)

                    if isinstance(response, ErrorBase):
                        await ws_manager.broadcast(response)
                    else:
                        RecorderService.record(db, validated)

                elif validated.type == 'poll':
                    await DeviceService.push_all_devices(db, ws_manager, websocket)

                elif validated.type == 'loop':
                    response = await LoopService.handle_websocket_command(
                        db, mqtt_client, ws_manager, validated.data)
                    await ws_manager.broadcast(response)
                else:
                    log.error(f"NOT IMPLEMENTED: {validated.type}")

            except ValidationError as e:
                error_response = ErrorBase(
                    error=type(e).__name__, message=e.__str__())
                await ws_manager.broadcast(error_response)

    except WebSocketDisconnect:
        ws_manager.disconnect(websocket)
