from api.schemas import SocketResponse
from pydantic import BaseModel
from fastapi import WebSocket
import logging
from api.database import SessionLocal, engine
from api.devices import models as device_models
from api.recorder.models import Record, Recording
from api.loop.models import ApplicationConfig
from fastapi_mqtt.fastmqtt import FastMQTT
from fastapi_mqtt.config import MQTTConfig
from api.config import mqtt_config as _mqtt_config
from logging.config import dictConfig
from fastapi.websockets import WebSocketDisconnect
from api.config import log_config

# Logging
dictConfig(log_config)
log = logging.getLogger('dirigent')

log.info("Application started")

# Configure MQTT
mqtt_config = MQTTConfig(**_mqtt_config)
mqtt = FastMQTT(config=mqtt_config)


class WebSocketManager:
    def __init__(self):
        self.active_connections: list[WebSocket] = []

    async def connect(self, websocket: WebSocket):
        await websocket.accept()
        self.active_connections.append(websocket)

    def disconnect(self, websocket: WebSocket):
        try:
            self.active_connections.remove(websocket)
        except ValueError as e:
            log.error("Cant remove a websocket again. ")

    async def send_personal_message(self, message: str, websocket: WebSocket):
        response = SocketResponse(type=message.type, data=message)

        log.debug(f"---> {response}")
        try:
            await websocket.send_json(response.json())
        except Exception as e:
            log.error(e)
            self.disconnect(websocket)

    async def broadcast(self, message: BaseModel):
        response = SocketResponse(type=message.type, data=message)
        log.debug(f"->>> {response}")

        for connection in self.active_connections:
            try:
                await connection.send_json(response.json())
            except Exception as e:
                log.error(e)
                self.disconnect(connection)


def get_mqtt_fastapi():
    return mqtt


def get_mqtt_client():
    return mqtt.client


ws_manager = WebSocketManager()


def get_ws_manager():
    _ws_manager = ws_manager
    return _ws_manager


device_models.Base.metadata.create_all(bind=engine)


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


application_state = {}


def get_state():
    return application_state
