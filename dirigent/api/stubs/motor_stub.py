import asyncio
from gmqtt import Client as MQTTClient
import gmqtt
from gmqtt.mqtt.constants import MQTTv311
import dotenv
import os
import sys
from stub_config import mqtt_config


class MotorStub():
    INTERVAL_TIME = 2

    def __init__(self, mqtt_id, reactive=False) -> None:
        self.mqtt_id = mqtt_id
        self.reactive = reactive

        # Set up the client
        self.client = MQTTClient(self.mqtt_id)

        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.set_auth_credentials(
            mqtt_config['username'], mqtt_config['password'])
        self.velocity = 0
        self.closing = False
        self.opening = False
        self.hit_bottom = False
        self.hit_top = False
        self.opening_since = 0
        self.closing_since = 0

    async def disconnect(self):
        self.client.publish('deregister', self.mqtt_id)
        await self.client.disconnect()

    async def connect(self):
        await self.client.connect(mqtt_config['host'], mqtt_config['port'], version=MQTTv311)

    def on_connect(self, client, flags, rc, properties):

        topics = [
            f"motor/{self.mqtt_id}/set_bottom_timeout",
            f"motor/{self.mqtt_id}/open",
            f"motor/{self.mqtt_id}/close",
            f"motor/{self.mqtt_id}/stop",
            "whosthere"
        ]
        for topic in topics:
            self.client.subscribe(topic, qos=1)
        self.client.publish('register/motor', self.mqtt_id)

    async def on_message(self, client, topic, payload, qos, properties):
        payload = int.from_bytes(payload, byteorder='little')
        if topic == "whosthere":
            self.client.publish('register/motor', self.mqtt_id)
        elif topic == f"motor/{self.mqtt_id}/stop":
            self.velocity = 0
            self.closing = False
            self.opening = False
        elif topic == f"motor/{self.mqtt_id}/open":
            if not self.hit_top:
                self.velocity = payload
                self.opening = True
                self.closing = False
        elif topic == f"motor/{self.mqtt_id}/close":
            if not self.hit_bottom:
                self.velocity = payload
                self.opening = False
                self.closing = True
        elif topic == f"motor/{self.mqtt_id}/set_bottom_timeout":
            self.client.publish(
                f'motor/{self.mqtt_id}/bottom_timeout', payload)

        if topic != 'whosthere' and topic != f'motor/{self.mqtt_id}/set_bottom_timeout':
            if self.opening:
                self.client.publish(
                    f"motor/{self.mqtt_id}/opening", str(self.velocity))
                if self.reactive:
                    await asyncio.sleep(MotorStub.INTERVAL_TIME)
                    self.publish_hit_top()
                    return
            if self.closing:
                self.client.publish(
                    f"motor/{self.mqtt_id}/closing", str(self.velocity))
                if self.reactive:
                    await asyncio.sleep(MotorStub.INTERVAL_TIME)
                    self.publish_hit_bottom()
                    return
            if not self.opening and not self.closing and not self.hit_bottom and not self.hit_top:
                self.client.publish(
                    f"motor/{self.mqtt_id}/stopped", str(self.velocity))

    def publish_hit_top(self):
        self.hit_bottom = False
        self.hit_top = True
        self.closing = False
        self.opening = False
        self.velocity = 0
        self.client.publish(
            f"motor/{self.mqtt_id}/hit_top", str(self.velocity))

    def publish_hit_bottom(self):
        self.hit_bottom = True
        self.hit_top = False
        self.closing = False
        self.opening = False
        self.velocity = 0
        self.client.publish(
            f"motor/{self.mqtt_id}/hit_bottom", str(self.velocity))


# Main loop to run the MQTT client
