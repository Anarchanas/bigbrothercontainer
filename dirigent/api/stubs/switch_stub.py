import asyncio
from gmqtt import Client as MQTTClient
import gmqtt
from gmqtt.mqtt.constants import MQTTv311
import json
from stub_config import mqtt_config


class SwitchStub():

    def __init__(self, mqtt_id) -> None:
        # Set up the client
        self.mqtt_id = mqtt_id
        self.client = MQTTClient(self.mqtt_id)
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.set_auth_credentials(
            mqtt_config['username'], mqtt_config['password'])
        self.turned_on = False

    async def disconnect(self):
        self.client.publish(f'deregister', self.mqtt_id)
        await self.client.disconnect()

    async def connect(self):
        await self.client.connect(mqtt_config['host'], mqtt_config['port'], version=MQTTv311)

    def on_connect(self, client, flags, rc, properties):
        self.client.subscribe(f"whosthere", qos=1)
        self.client.subscribe(f"switch/{self.mqtt_id}/turn_on", qos=1)
        self.client.subscribe(f"switch/{self.mqtt_id}/turn_off", qos=1)
        self.client.subscribe(f"switch/{self.mqtt_id}/toggle", qos=1)
        self.client.publish(f'register/switch', self.mqtt_id, qos=1)

    def advertise_status(self):
        if self.turned_on:
            self.client.publish(f"switch/{self.mqtt_id}/on")
        else:
            self.client.publish(f"switch/{self.mqtt_id}/off")

    async def on_message(self, client, topic, payload, qos, properties):
        if topic == 'whosthere':
            self.client.publish('register/switch', self.mqtt_id, qos=1)
            return
        if topic.split('/')[-1] == 'turn_on':
            self.turned_on = True
        elif topic.split('/')[-1] == 'turn_off':
            self.turned_on = False
        elif topic.split('/')[-1] == 'toggle':
            self.turned_on = not self.turned_on

        self.advertise_status()
