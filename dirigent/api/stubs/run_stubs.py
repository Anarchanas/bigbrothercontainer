
from light_stub import LightStub
from motor_stub import MotorStub
from switch_stub import SwitchStub
from stub_config import backend_url
import asyncio
from gmqtt import Client as MQTTClient
import gmqtt
from gmqtt.mqtt.constants import MQTTv311
import dotenv
import os
import sys
import selectors
import random
import requests


async def wait_for_keypress():
    '''Wait for a keypress asynchronously.'''
    selector = selectors.DefaultSelector()

    # Use the fileno() of sys.stdin for registration
    key = selector.register(sys.stdin.fileno(), selectors.EVENT_READ)

    # Wait for input event
    await loop.run_in_executor(None, selector.select)

    # Clean up by using the fileno() again
    selector.unregister(sys.stdin.fileno())
    selector.close()


def send_details(mqtt_id):
    rooms = ['Kitchen', 'Bath', 'Sleep', 'Living', 'Confession']
    url = f'{backend_url}/api/devices/{mqtt_id}'
    headers = {
        'Content-Type': 'application/json',  # or another content type if necessary
    }
    data = {
        'name': mqtt_id,
        'group': random.choice(rooms),  # "stubs",
    }
    response = requests.put(url, json=data, headers=headers)
    return response


async def main():
    stubs = []
    stub_ids = []
    for i in range(13):
        mqtt_id = f"motor_stub_{i}"
        stubs.append(MotorStub(mqtt_id, reactive=True))
        stub_ids.append(mqtt_id)

    for i in range(50):
        mqtt_id = f"light_stub_{i}"
        stubs.append(LightStub(mqtt_id))
        stub_ids.append(mqtt_id)

    for i in range(10):
        mqtt_id = f"switch_stub_{i}"
        stubs.append(SwitchStub(mqtt_id))
        stub_ids.append(mqtt_id)

    for stub in stubs:
        await stub.connect()

    await asyncio.sleep(1)
    for id in stub_ids:
        send_details(id)

    print("Press any key to continue...")
    await wait_for_keypress()

    for stub in stubs:
        await stub.disconnect()

# To run the script
if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
