

import pytest
from api.stubs.motor_stub import MotorStub
from fastapi.testclient import TestClient
from fastapi import WebSocket
from api.test_fixtures import get_db, get_ws_manager
import httpx
import websockets
import json
import asyncio
from api.devices.motors.schemas import MotorCommand
from api.websocket import SocketRequest

BACKEND_URL = 'http://localhost:8000'
WEBSOCKET_URL = 'ws://localhost:8000/ws'
MOTOR_MQTT_ID = 'test_1'


async def receiver(websocket, queue):
    async for message in websocket:
        await queue.put(message)


def socket_request(command, type):
    return SocketRequest(type=type, data=command).json()


async def get_json(queue: asyncio.Queue):
    await asyncio.sleep(0.3)
    msg = await queue.get()
    msg = json.loads(msg)
    msg = json.loads(msg)
    return msg


@pytest.mark.asyncio
async def test_register_deregister_motor():
    async with httpx.AsyncClient() as client:
        async with websockets.connect(WEBSOCKET_URL) as ws:
            response = await client.delete(BACKEND_URL + "/api/devices/")
            assert response.status_code == 204

            queue = asyncio.Queue()
            asyncio.create_task(receiver(ws, queue))

            assert queue.qsize() == 0
            http_response = await client.get(BACKEND_URL + "/api/devices/")
            assert http_response.status_code == 204
            assert queue.qsize() == 0
            stubs = []
            for i in range(5):
                m = MotorStub(f'tester_{i}')
                stubs.append(m)
                await m.connect()
                msg = await get_json(queue)
                assert msg['data']['mqtt_id'] == f'tester_{i}'

            assert queue.qsize() == 0

            http_response = await client.get(BACKEND_URL + "/api/devices/")
            for i in range(5):
                msg = await get_json(queue)
                assert msg['data']['mqtt_id'] == f'tester_{i}'

            for i in range(5):
                await stubs[i].disconnect()
                msg = await get_json(queue)
                assert msg['data']['mqtt_id'] == f'tester_{i}'
            assert queue.qsize() == 0

            response = await client.delete(BACKEND_URL + "/api/devices/")
            await asyncio.sleep(0.2)

            response = await client.get(BACKEND_URL + "/api/devices/")
            assert queue.qsize() == 0

            motor = MotorStub(MOTOR_MQTT_ID)
            await motor.connect()
            # We should get a push notification for its status.
            await asyncio.sleep(0.2)
            assert queue.qsize() == 1
            await client.post(BACKEND_URL + "/api/motors/hit_bottom_timeout/2000")

            http_response = await client.get(BACKEND_URL + "/api/devices/")
            await asyncio.sleep(0.2)
            assert queue.qsize() == 2

            # We should get a push notification for the disconnect.
            await motor.disconnect()
            await asyncio.sleep(0.2)
            assert queue.qsize() == 3

            # response = await client.delete(BACKEND_URL + "/api/devices", follow_redirects=True)
            # http_response = await client.get(BACKEND_URL + "/api/devices", follow_redirects=True)
            # assert queue.qsize() == 2
            # http_response = await client.get(BACKEND_URL + "/api/devices", follow_redirects=True)
            i = 0
            connected = [True, True, False]
            bottom_timeout = [0, 2000, 2000]
            while not queue.empty():
                message = await queue.get()
                message = json.loads(message)
                message = json.loads(message)
                assert message['data']['connected'] == connected[i]
                assert message['data']['bottom_timeout'] == bottom_timeout[i]
                i += 1


@pytest.mark.asyncio
async def test_stop_close_open_motor():
    async with httpx.AsyncClient() as client:
        async with websockets.connect(WEBSOCKET_URL) as ws:
            response = await client.delete(BACKEND_URL + "/api/devices/")
            queue = asyncio.Queue()
            asyncio.create_task(receiver(ws, queue))
            motor = MotorStub(MOTOR_MQTT_ID)
            await motor.connect()
            msg = await get_json(queue)
            assert msg['data']['connected']

            cmd = MotorCommand(
                command='stop', mqtt_id=MOTOR_MQTT_ID, velocity=0)

            await ws.send(socket_request(cmd, 'motor'))
            await asyncio.sleep(0.2)

            msg = await get_json(queue)
            assert not msg['data']['opening'] and not msg['data']['closing']
            assert queue.qsize() == 0

            cmd = MotorCommand(
                command='open', mqtt_id=MOTOR_MQTT_ID, velocity=255)
            await ws.send(socket_request(cmd, 'motor'))
            await asyncio.sleep(0.2)
            msg = await get_json(queue)
            assert queue.qsize() == 0

            assert msg['data']['opening'] and not msg['data']['closing']
            assert msg['data']['velocity'] == 255

            cmd = MotorCommand(
                command='close', mqtt_id=MOTOR_MQTT_ID, velocity=255)
            await ws.send(socket_request(cmd, 'motor'))
            await asyncio.sleep(0.2)
            msg = await get_json(queue)
            assert queue.qsize() == 0
            assert not msg['data']['opening'] and msg['data']['closing']
            assert msg['data']['velocity'] == 255

            # Make sure that its not possible when a device is disconnected
            await motor.disconnect()
            await get_json(queue)
            await ws.send(socket_request(cmd, 'motor'))
            msg = await get_json(queue)
            assert queue.qsize() == 0
            assert msg['type'] == 'error'


@pytest.mark.asyncio
async def test_hit_top_bottom():
    async with httpx.AsyncClient() as client:
        async with websockets.connect(WEBSOCKET_URL) as ws:
            response = await client.delete(BACKEND_URL + "/api/devices/")
            queue = asyncio.Queue()
            asyncio.create_task(receiver(ws, queue))
            motor = MotorStub(MOTOR_MQTT_ID)
            await motor.connect()
            # We should get a push notification for its status.
            msg = await get_json(queue)
            assert msg['data']['connected']
            assert not msg['data']['on_bottom'] and not msg['data']['on_top']

            motor.publish_hit_bottom()
            msg = await get_json(queue)
            assert msg['data']['on_bottom'] and not msg['data']['on_top']

            motor.publish_hit_top()
            msg = await get_json(queue)
            assert not msg['data']['on_bottom'] and msg['data']['on_top']

            await motor.disconnect()
            msg = await get_json(queue)
            assert not msg['data']['connected']
