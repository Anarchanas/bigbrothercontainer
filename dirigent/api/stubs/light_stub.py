import asyncio
from gmqtt import Client as MQTTClient
import gmqtt
from gmqtt.mqtt.constants import MQTTv311
import json
from stub_config import mqtt_config


class LightStub():

    def __init__(self, mqtt_id) -> None:
        # Set up the client
        self.mqtt_id = mqtt_id
        self.client = MQTTClient(self.mqtt_id)
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.set_auth_credentials(
            mqtt_config['username'], mqtt_config['password'])
        self.preset_active = 1
        self.brightness = 0
        self.temperature = 0

    async def disconnect(self):
        self.client.publish(f'wled/{self.mqtt_id}/status', 'offline')
        await self.client.disconnect()

    async def connect(self):
        await self.client.connect(mqtt_config['host'], mqtt_config['port'], version=MQTTv311)

    def on_connect(self, client, flags, rc, properties):
        self.client.subscribe(f"wled/{self.mqtt_id}/api", qos=1)
        self.client.publish(f'wled/{self.mqtt_id}/status', 'online')

    async def on_message(self, client, topic, payload, qos, properties):
        payload = json.loads(payload)
        if "seg" in payload.keys():
            segment_cmds = payload['seg']  # list of commands
            for cmd in segment_cmds:
                if "cct" in cmd:
                    self.temperature = cmd['cct']

        if "bri" in payload.keys():
            self.brightness = payload['bri']
        if "ps" in payload.keys():
            self.preset_active = payload['ps']

        self.client.publish(f'wled/{self.mqtt_id}/status', 'online')
