import dotenv
import os
import sys
import selectors

BBCONTAINER_ENV_FILE = os.getenv('BBCONTAINER_ENV_FILE')
if BBCONTAINER_ENV_FILE is None:
    dotenv.load_dotenv(dotenv_path='../.env', verbose=True)
    print("LOADED .env")
else:
    dotenv.load_dotenv(dotenv_path=f'../{BBCONTAINER_ENV_FILE}', verbose=True)
    print(f"LOADED {BBCONTAINER_ENV_FILE}")

backend_url = 'http://192.168.50.85'

mqtt_config = {
    "username": os.getenv('MQTT_USER'),
    "password": os.getenv('MQTT_PASSWORD'),
    # "host": 'localhost',
    "host": '192.168.50.85',

    "port": int(os.getenv('MQTT_PORT')),
    "reconnect_retries": -1,  # -1 try it forever
    "reconnect_delay": 6,
}
