# Lights

Here we simply collect all the information regarding the lights.

## LEDVANCE SMARTWF LEDTUBE T8EM 1200

- [Product Sheet](https://www.ledvance.de/de/product-datasheet/181786/168041)
- [Installation Guide](https://www.ledvance.de/media/resource/original/asset-13103846)

### Connectivity

Mobile app to manipulate lights. WIFI only. Can be connected to Google Assistant or Amazon Alexa. 

LEDVANCE do not seem to be very accessible for open source signalling methods. There is a open source system called [Homebridge](https://github.com/homebridge/homebridge) which has many plugins however LEDVANCE is not among them. Check [this thread](https://www.reddit.com/r/homebridge/comments/kfnjev/homebridge_plugin_for_sylvania_ledvance_smart/) for more information.

## WLED

Here, you can find the resources related to WLED:

- [Installation Guide](https://kno.wled.ge/basics/getting-started/): This guide covers the installation of PixelLEDS in general.
- [List of Compatible LED Strips](https://kno.wled.ge/basics/compatible-led-strips/): Explore this list to find LED strips compatible with WLED.
- [WLED Multi-Strip Support](https://kno.wled.ge/features/multi-strip/): Use this file for calculating the number of Arduinos needed and to learn how to trigger WLED over [MQTT](https://kno.wled.ge/interfaces/mqtt/).

## PixelLEDS

Here are the different types of PixelLEDs ordered for the prototypes:

- [RGBWW DC24V FW1906 (not fully supported 01.09.23) Product Sheet](https://de.aliexpress.com/item/1005004614473019.html?spm=a2g0o.order_list.order_list_main.9.75775c5fAjBirH&gatewayAdapt=glo2de)
  - [GitHub Pull for WLED of FW1906](https://github.com/Aircoookie/WLED/pull/3298): View the GitHub Pull Request related to FW1906.
- [WWA DC5V SK6812 (supported) Product Sheet](https://de.aliexpress.com/item/32840279834.html?spm=a2g0o.order_list.order_list_main.10.75775c5fAjBirH&gatewayAdapt=glo2deu)
- [FCOB SPI CCT DC24V WS2811 (supported) Product Sheet](https://de.aliexpress.com/item/1005005069644213.html?spm=a2g0o.order_list.order_list_main.15.75775c5fAjBirH&gatewayAdapt=glo2deu)
- [RGBNW DC5V SK6812 (supported) Product Sheet](https://de.aliexpress.com/item/32476317187.html?spm=a2g0o.order_list.order_list_main.14.75775c5fAjBirH&gatewayAdapt=glo2deu)


